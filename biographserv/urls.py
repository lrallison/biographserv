from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from bgs import views as bgs_views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'biographserv.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', bgs_views.index, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^bgs/', include('bgs.urls')),
    url(r'^chaining/', include('pixelfields_smart_selects.urls')),
)

# Fix for gunicorn setup
urlpatterns += staticfiles_urlpatterns()
