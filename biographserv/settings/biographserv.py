import os

DEBUG = False
ALLOWED_HOSTS = ['biographserv.com'] # Needed when debug=False
IGENOMES_DIR = os.path.join("/mnt/reference/iGenomes")
GROUP_NAME = 'biographserv'