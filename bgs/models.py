import StringIO
from collections import OrderedDict
from django.db import models
from django.db.models.aggregates import Min
from django.db.models.deletion import SET_NULL
import grp
import hashlib
from model_utils.managers import InheritanceManager
from operator import attrgetter
import os
import pickle
from pixelfields_smart_selects.db_fields import ChainedForeignKey
import stat

from bgs import genomics
from bgs.utils import matplotlib_utils
from bgs.utils.django_utils import delimited_file_has_header, \
    get_subclass_or_create
from bgs.utils.file_utils import get_extension, mk_path_for_file, mk_path, \
    name_from_file_name
from biographserv import settings
import pandas as pd
from reference_server.genomics import open_handle_gz
from reference_server.reference.igenomes_reference import IGenomesReference


class Organism(models.Model):
    species_name = models.CharField(max_length=256)
    common_name = models.CharField(max_length=256)
    def __unicode__(self):
        return "%s (%s)" % (self.species_name, self.common_name)

class AnnotationGroup(models.Model):
    name = models.CharField(max_length=256)
    def __unicode__(self):
        return self.name

class Build(models.Model):
    organism = models.ForeignKey(Organism)
    annotation_group = models.ForeignKey(AnnotationGroup)
    name = models.CharField(max_length=256)

    def get_igenomes_reference(self):
        return IGenomesReference(self.organism.species_name, self.annotation_group.name, self.name)

    def __unicode__(self):
        return "%s/%s" % (self.annotation_group, self.name)

class Project(models.Model):
    MAX_EMAIL_LENGTH=256
    uuid = models.CharField(max_length=32, primary_key=True)
    organism = models.ForeignKey(Organism, null=True)
#    annotation_group = models.ForeignKey(AnnotationGroup, null=True)
    build = ChainedForeignKey(Build,
                              chained_field='organism',
                              chained_model_field='organism',
                              show_all=False,
                              auto_choose=True,
                              null=True)
    name = models.CharField(max_length=100)
    description = models.TextField()
    email = models.CharField(max_length=MAX_EMAIL_LENGTH)
    read_only = models.BooleanField(default=False)
    
    @property
    def date(self):
        # Earliest uploaded file
        return self.uploadedfile_set.all().aggregate(Min('date')).values()[0]

    def get_combos(self):
        combo_dicts = []
        for (combo_id, name) in self.combo_set.all().order_by("id").values_list("id", "name"):
            combo_data = {"id" : combo_id, "name" : name}
            combo_dicts.append(combo_data)
        return combo_dicts

    def create_default_genomic_ranges(self):
        GenomicRange.objects.create(project=self,
                                name='Promoter',
                                anchor=GenomicAnchor.TSS,
                                upstream=1000,
                                downstream=1000,
                                max_size=3000,
                                step_size=100,)

        GenomicRange.objects.create(project=self,
                                    name='Gene Body',
                                    anchor=GenomicAnchor.GENE_START_END,
                                    upstream=0,
                                    downstream=0,
                                    max_size=20000,
                                    step_size=500,)


    def __unicode__(self):
        name = self.name or self.pk
        if self.build:
            name = "%s (%s)" % (name, self.build)
        return name


class ComboType(object):
    REGION_STATS = 'R'
    GENES = 'G'
    OTHER = 'O'
    COMBO_TYPE_CHOICE = (
        (REGION_STATS, 'Region Stats'),
        (GENES, 'Genes'),
        (OTHER, 'Other'),
    )

class Combo(models.Model):
    project = models.ForeignKey(Project)
    name = models.CharField(max_length=100)
    combo_type = models.CharField(max_length=1, choices=ComboType.COMBO_TYPE_CHOICE, null=True, blank=True)
    file_name = models.TextField()
    
    def get_columns(self):
        if self.combo_type is None:
            return ([], []) 
        
        csv_results_qs = CSVResult.get_for_project(self.project)
        if self.combo_type == ComboType.REGION_STATS:
            columns_qs = csv_results_qs.filter(result_type=CSVResultType.REGION_STATS)
        else:
            result_types = [CSVResultType.GENE_SCORES]
            result_types.extend(CSVResultType.UPLOADED_CSVRESULTS)
            columns_qs = csv_results_qs.filter(result_type__in=result_types)
        
        selected_column_ids = self.combocolumn_set.all().values_list("csv_result_id", flat=True)
        selected_columns = CSVResult.objects.filter(id__in=selected_column_ids).order_by("combocolumn__sort_order")
        available_columns = columns_qs.exclude(id__in=selected_column_ids)
        return (available_columns, selected_columns)

    def get_dataframe(self):
        dataframes = []
        for cc in self.combocolumn_set.all().order_by("sort_order"):
            cc_df = cc.csv_result.get_dataframe()
            dataframes.append(cc_df)
            
        df = pd.concat(dataframes, axis=1)
        return df

    def get_column_types_and_homogeneity(self):
        ''' returns (num_column_types, distinct_column_types, is_homogenous) '''

        mappings = get_csv_result_type_mappings()
        column_types = list(self.combocolumn_set.all().values_list('csv_result__result_type', flat=True))

        num_column_types = len(column_types)
        distinct_column_types = set(mappings[t] for t in column_types) 
        is_homogenous = len(distinct_column_types) <= 1

        return (num_column_types, distinct_column_types, is_homogenous)
        
    def is_multi_expression(self):
        (num_column_types, distinct_column_types, is_homogenous) = self.get_column_types_and_homogeneity()
        return num_column_types > 1 and is_homogenous and "Expression" in distinct_column_types

    
    def __unicode__(self):
        return "Combo: %s%s" % (self.name or '', self.project)

class GenomicAnchor(object):
    TSS = 'P'
    GENE_START_END = 'G'
    GENOMIC_ANCHOR_CHOICE = (
        (TSS, 'TSS'),
        (GENE_START_END, 'Gene Start/End')
    )

class GenomicRange(models.Model):
    project = models.ForeignKey(Project)
    name = models.TextField()
    anchor = models.CharField(max_length=1, choices=GenomicAnchor.GENOMIC_ANCHOR_CHOICE)
    upstream = models.IntegerField()
    downstream = models.IntegerField()
    max_size = models.IntegerField()
    step_size = models.IntegerField()


def get_processing_dir(project_id):
    processing_dir = os.path.join(settings.PROCESSOR_OUTPUT_DIR, str(project_id))
    mk_path(processing_dir)
    posix_stat = os.stat(processing_dir)

    if settings.GROUP_NAME:
        try:
            uid = posix_stat.st_uid
            gid = grp.getgrnam(settings.GROUP_NAME).gr_gid
    #        print "Changing permissions:"
            os.chown(processing_dir, uid, gid)
    #        print "Changing group ownership:"
            os.chmod(processing_dir, posix_stat.st_mode | stat.S_IWGRP)
        except Exception as e:
            print e # Ignore
#    posix_stat = os.stat(processing_dir)
#    mode = posix_stat.st_mode
    
#    print "%s mode: %d" % (processing_dir, mode)
#    if not stat.S_IWGRP & mode:
#        print "Not writable by GROUP" 
#    if not stat.S_IWOTH & mode:
#        print "Not writable by others" 
    
    return processing_dir

class UploadedFileTypes(object):
    BED = 'B'
    VCF = 'V'
    CUFFDIFF = 'C'
    EXPRESSION = 'E'
    SINGLE_SAMPLE_EXPRESSION = 'S'
    MULTI_SAMPLE_EXPRESSION = 'M'
    UPLOADED_FILES_CHOICE = (
        (BED, 'BED'),
        (VCF, 'VCF'),
        (CUFFDIFF, 'CuffDiff'),
        (SINGLE_SAMPLE_EXPRESSION, 'Expression (single sample)'),
        (MULTI_SAMPLE_EXPRESSION, 'Expression (multi-sample)'),
    )

    EXTENSIONS = {BED : ['bed', 'broadPeak', 'narrowPeak'],
                  VCF : ['vcf'],
                  EXPRESSION : ['tsv', 'csv'],
                  CUFFDIFF : ['diff']}

    REQUIRES_REFERENCE = {BED}
    REQUIRES_GENOMIC_RANGES = {BED}

    @classmethod
    def get_file_type_from_extension(cls, uploaded_file):
        extension_lookup = cls.get_extension_lookup()

        filename = uploaded_file.name
        filename, open_file = genomics.handle_open_file_gzip(filename, uploaded_file.file)
        ext = get_extension(filename)
        try:
            file_type = extension_lookup[ext]
        except KeyError as e:
            print e
            msg = "Can't handle files with extension '%s'" % ext
            raise ValueError(msg)
            
        if file_type == UploadedFileTypes.EXPRESSION:
            file_type = cls.determine_expression_type(filename, open_file) 
        
        return file_type

    @classmethod
    def get_extension_lookup(cls):
        extension_lookup = {}
        for (file_type, extensions) in cls.EXTENSIONS.iteritems():
            for ext in extensions:
                extension_lookup[ext] = file_type
        return extension_lookup

    @classmethod
    def determine_expression_type(cls, file_name, open_file):
        (has_header, num_columns, _) = delimited_file_has_header(open_file)
        if num_columns == 2 and not has_header:
            return cls.SINGLE_SAMPLE_EXPRESSION
        elif num_columns > 2 and has_header:
            return cls.MULTI_SAMPLE_EXPRESSION
        
        msg = "Could not determine expression type of %s header: %s, columns: %d" % (file_name, has_header, num_columns)
        raise ValueError(msg)

    @classmethod
    def requires_reference(cls, uploaded_file_type):
        return uploaded_file_type in cls.REQUIRES_REFERENCE


class UploadedFile(models.Model):
    project = models.ForeignKey(Project)
    name = models.TextField()
    uploaded_file = models.FileField(upload_to='uploads', max_length=256)
    file_type = models.CharField(max_length=1, choices=UploadedFileTypes.UPLOADED_FILES_CHOICE)
    date = models.DateTimeField()
    def get_file(self):
        ''' returns the actual file on the server '''
        return self.uploaded_file.file.file

    def uses_reference(self):
        return UploadedFileTypes.requires_reference(self.file_type)

    def needs_reference_set(self):
        return self.uses_reference() and self.project.build is None

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.get_file_type_display())

class ProcessingStatus(object):
    CREATED = 'C'
    PROCESSING = 'P'
    ERROR = 'E'
    SUCCESS = 'S'
    STATUS_CHOICE = (
        (CREATED, 'Created'),
        (PROCESSING, 'Processing'),
        (ERROR, 'Error'),
        (SUCCESS, 'Success'),
    )
    INCOMPLETE = (CREATED, PROCESSING)


# TODO: The best way to do this is as subclass which is either a combo or uploaded file
# Rather than having 1 of the fields be null.
# Trouble is, migration is hard. Wait until server doesn't need to be up, trash and restart new way 
class FileProcessingTask(models.Model):
    name = models.TextField()
    uploaded_file = models.ForeignKey(UploadedFile, null=True)
    status = models.CharField(max_length=1, choices=ProcessingStatus.STATUS_CHOICE)
    error_message = models.TextField()
    created_date = models.DateTimeField()
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    items_processed = models.IntegerField(default=0)
    celery_task = models.CharField(max_length=36, null=True)
    combo = models.ForeignKey(Combo, null=True)

    @property
    def processing_seconds(self):
        if self.start_date and self.end_date:
            seconds = (self.end_date - self.start_date).total_seconds()
        else:
            seconds = None
        return seconds
    
    def get_project(self):
        if self.combo:
            return self.combo.project
        elif self.uploaded_file:
            return self.uploaded_file.project
        else:
            raise ValueError("%d has neither combo or uploaded_file set" % (self.pk))

    def get_base_name(self):
        ''' Used for any produced files '''
        if self.uploaded_file:
            return name_from_file_name(self.uploaded_file.uploaded_file.path)
        elif self.combo:
            return "combo_%s" % self.combo.name
    
    def uses_reference(self):
        if self.uploaded_file:
            return self.uploaded_file.uses_reference()
        else:
            return False
    
    def get_reference(self):
        return self.get_project().build.get_igenomes_reference()
    
    def get_results(self):
        results = []
        for result in self.processingresult_set.select_subclasses().order_by('-id'):
            if result.get_display_type() == ProcessingResult.IMAGE:
                styled_graph = StyledGraph.objects.get_subclass(graph=result)
                results.append(styled_graph)
            elif result.get_display_type() == ProcessingResult.CSV:
                results.append(result)
        return results
    
    
    def __unicode__(self):
        if self.combo:
            desc = 'Combo: %s' % self.combo
        elif self.uploaded_file:
            desc = 'UploadedFile: %s' % self.uploaded_file

        return "FileProcessingTask: %s (%s/%s)" % (self.name, desc, self.status)


class ProcessingResult(models.Model):
    IMAGE = 'I'
    CSV = 'C'
    
    name = models.TextField()
    file_processing_task = models.ForeignKey(FileProcessingTask)
    objects = InheritanceManager()

    def get_display_type(self):
        raise NotImplementedError("ProcessingResult is an abstract class")

    def is_image(self):
        return self.get_display_type() == self.IMAGE

    def is_csv(self):
        return self.get_display_type() == self.CSV


class Graph(ProcessingResult):
    serializable_graph = None
    serializable_graph_blob = models.BinaryField()

    def __init__(self, *args, **kwargs):
        self.serializable_graph = kwargs.pop("serializable_graph", None)
        super(Graph, self).__init__(*args, **kwargs)

    def get_display_type(self):
        return self.IMAGE

    def save(self, *args, **kwargs):
        if self.serializable_graph:
            self.serializable_graph_blob = pickle.dumps(self.serializable_graph)        

        super(Graph, self).save(*args, **kwargs)

        graph_class = self.serializable_graph.get_styled_graph_class()
        styled_graph, created = get_subclass_or_create(StyledGraph, graph_class, graph=self)

        if created:
            styled_graph.set_from_graph(self.serializable_graph)
            styled_graph.save()
            styled_graph.write_image()

    @staticmethod
    def get_for_project(project_id):
        return Graph.objects.filter(file_processing_task__uploaded_file__project=project_id)


def load_single_column_df(file_name, name=None):
    kwargs = {'header' : None, 'sep' : None}
    open_file = open_handle_gz(file_name)
    
    if name is None:
        df = pd.DataFrame.from_csv(open_file, **kwargs)
    else:
        df = pd.read_table(open_file, names=[name], **kwargs)
    return df

def load_multi_column_df(file_name, name=None):
    ''' Name not used '''
    open_file = open_handle_gz(file_name)
    return pd.DataFrame.from_csv(open_file, sep=None)

class CSVResultType(object):
    REGION_STATS = 'R'
    GENE_SCORES = 'G'
    READ_COUNTS = 'T' # t for tags
    SINGLE_SAMPLE_EXPRESSION = 'S'
    MULTI_SAMPLE_EXPRESSION = 'M'
    CUFFDIFF = 'C'
    OTHER = 'O'
    CSV_RESULT_TYPE_CHOICE = (
        (REGION_STATS, 'Region Stats'),
        (GENE_SCORES, 'Bed Gene Scores'),
        (READ_COUNTS, 'Read Counts'),
        (OTHER, 'Other'),
        (SINGLE_SAMPLE_EXPRESSION, 'Uploaded Single Sample Expression'),
        (MULTI_SAMPLE_EXPRESSION, 'Uploaded Multi Sample Expressions'),
        (CUFFDIFF, 'Uploaded CuffDiff'),
    )
    # These get results automatically made to download
    UPLOADED_CSVRESULTS = set([CUFFDIFF, SINGLE_SAMPLE_EXPRESSION, MULTI_SAMPLE_EXPRESSION])

    LOADERS = {GENE_SCORES : load_single_column_df,
               REGION_STATS : load_single_column_df,
               SINGLE_SAMPLE_EXPRESSION : load_single_column_df,
               MULTI_SAMPLE_EXPRESSION : load_multi_column_df,
               CUFFDIFF : load_multi_column_df,
    }

def get_csv_result_type_mappings():
    ''' Basically a way to have expression types map the same '''
    mappings = {k : k for (k,_) in CSVResultType.CSV_RESULT_TYPE_CHOICE}
    mappings[CSVResultType.SINGLE_SAMPLE_EXPRESSION] = 'Expression'
    mappings[CSVResultType.MULTI_SAMPLE_EXPRESSION] = 'Expression'
    return mappings
    

class CSVResult(ProcessingResult):
    file_name = models.TextField()
    result_type = models.CharField(max_length=1, choices=CSVResultType.CSV_RESULT_TYPE_CHOICE)

    def get_display_type(self):
        return self.CSV

    def __unicode__(self):
        name = self.file_processing_task.get_base_name()
        if self.name:
            name = "%s: %s" % (name, self.name)
        return name

    def get_dataframe(self):
        loader = CSVResultType.LOADERS[self.result_type]
        name = str(self)
        return loader(self.file_name, name=name)

    @staticmethod
    def get_for_project(project_id):
        return CSVResult.objects.filter(file_processing_task__uploaded_file__project=project_id)

    @staticmethod
    def get_for_project_and_result_type(project_id, result_type):
        return CSVResult.objects.filter(file_processing_task__uploaded_file__project_id=project_id, result_type=result_type)

class CSVColumn(models.Model):
    csv = models.ForeignKey(CSVResult)
    sort_order = models.IntegerField()
    column_name = models.TextField()


class ComboColumn(models.Model):
    combo = models.ForeignKey(Combo)
    sort_order = models.IntegerField()
    csv_result = models.ForeignKey(CSVResult)


class StyledGraph(models.Model):
    objects = InheritanceManager()
    graph = models.ForeignKey(Graph)
    hash_code = models.CharField(max_length=40)

    # These we copy from SerialisableGraph
    GRAPH_FIELDS = ['title',
                    'cmap',
                    'title',
                    'xlabel',
                    'ylabel',
                    'top_axis',
                    'bottom_axis',
                    'left_axis',
                    'right_axis',
                    'xgrid',
                    'ygrid',
    ]
    
    title = models.TextField(null=True, blank=True)
    color = models.CharField(max_length=7, default='#ff0000')
    cmap = models.TextField(null=True, blank=True)
    xlabel = models.TextField(null=True, blank=True)
    ylabel = models.TextField(null=True, blank=True)
    top_axis = models.BooleanField(default=True)
    bottom_axis = models.BooleanField(default=True)
    left_axis = models.BooleanField(default=True)
    right_axis = models.BooleanField(default=True)
    xgrid = models.BooleanField(default=False)
    ygrid = models.BooleanField(default=False)

    number_of_colors = models.IntegerField(default=0)
    has_colormap = models.BooleanField(default=False)

    def get_project_id(self):
        return self.graph.file_processing_task.get_project().pk

    def get_filename(self, image_type):
        file_name = self.hash_code + '.' + image_type
        return os.path.join(get_processing_dir(self.get_project_id()), "style_graphs", str(self.pk), file_name)

    def _get_hashcode_fields(self):
        return [str(getattr(self, f)) for f in self.GRAPH_FIELDS]

    def _get_hashcode(self):
        sha1 = hashlib.sha1()
        sha1.update(str(self.graph.pk))
        for f in self._get_hashcode_fields():
            sha1.update(f)

        sha1.update(self.color)
        return sha1.hexdigest()

    def save(self, *args, **kwargs):
        self.hash_code = self._get_hashcode()
        super(StyledGraph, self).save(*args, **kwargs)

    def has_colors(self):
        return self.number_of_colors > 0

    def set_from_graph(self, serialisable_graph):
        for f in self.GRAPH_FIELDS:
            value = getattr(serialisable_graph, f)
            setattr(self, f, value)
        self.color = serialisable_graph.colors[0]

        # Note we don't set these going the other way as they are built into the classes...
        self.number_of_colors = serialisable_graph.number_of_colors()
        self.has_colormap = serialisable_graph.has_colormap()
        
        print "%s: %d colors (%s)" % (self.pk, self.number_of_colors, serialisable_graph.get_colors_description())
        print "%s colormap: %r" % (self.pk, self.has_colormap)

    def set_graph_values(self, serialisable_graph):
        for f in self.GRAPH_FIELDS:
            value = getattr(self, f)
            setattr(serialisable_graph, f, value)
        serialisable_graph.colors = [self.color]
        ax = serialisable_graph.axes()
        serialisable_graph._apply_styles(ax)

    def write_image(self, file_name=None, image_type='png'):
        if file_name is None:
            file_name = self.get_filename(image_type)
            
        obj = pickle.loads(self.graph.serializable_graph_blob)
        self.set_graph_values(obj)
        mk_path_for_file(file_name)

        string_buffer = StringIO.StringIO()
        obj.save(string_buffer, image_type)
        image = string_buffer.getvalue()
        with open(file_name, "w") as f:
            f.write(image)
        return image

    def get_image(self, image_type):
        file_name = self.get_filename(image_type)
        if not os.path.exists(file_name):
            print "Having to write"
            image = self.write_image(file_name, image_type)
        else:
            print "Already exists"
    
            with open(file_name) as f:
                image = f.read()

        return image

    def get_categories_dict(self):
        categories_dicts = []
        for (category_id, name) in self.graphcategory_set.all().order_by("id").values_list("id", "name"):
            category_data = {"id" : category_id, "name" : name}
            categories_dicts.append(category_data)
        return categories_dicts

    def is_image(self):
        return True
    
    def is_csv(self):
        return False
    
    def get_editor_template(self):
        return 'bgs/graph_editors/styled_graph_editor.html'


class GraphSample(models.Model):
    styled_graph = models.ForeignKey(StyledGraph)
    name = models.TextField()
    color = models.CharField(max_length=7, null=True)

    def __unicode__(self):
        return self.name

class GraphCategory(models.Model):
    styled_graph = models.ForeignKey(StyledGraph)
    name = models.TextField()
    cmap = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        super(GraphCategory, self).save(*args, **kwargs)
        
        # Make sure we have a GSGR for each sample
        for graph_sample in self.styled_graph.graphsample_set.all():
            GraphSampleGroupRelationship.objects.get_or_create(graph_sample=graph_sample,
                                                               graph_category=self)

    def get_project(self):
        return self.styled_graph.graph.file_processing_task.get_project()

    def get_sample_groups_and_colors(self):
        graph_sample_qs = self.graphsamplegrouprelationship_set.all()
        sample_groups = OrderedDict()
        groups = set()
        for gsgr in graph_sample_qs:
            groups.add(gsgr.graph_group)
            sample_groups[gsgr.graph_sample.name] = gsgr.graph_group.name

        groups = sorted(groups, key=attrgetter('id'))
        
        if self.cmap:
            colors = matplotlib_utils.get_colors_from_color_map(len(groups), self.cmap, False)
        else:
            colors = [g.color for g in groups]
        
        group_colors = OrderedDict()
        for (i, graph_group) in enumerate(groups):
            group_colors[graph_group.name] = colors[i]

        return (sample_groups, group_colors)


    def __unicode__(self):
        return self.name

class GraphGroup(models.Model):
    graph_category = models.ForeignKey(GraphCategory)
    name = models.TextField()
    color = models.CharField(max_length=7, null=True)

    def __unicode__(self):
        return self.name

class GraphSampleGroupRelationship(models.Model):
    graph_sample = models.ForeignKey(GraphSample)
    graph_category = models.ForeignKey(GraphCategory)
    graph_group = models.ForeignKey(GraphGroup, null=True, on_delete=SET_NULL)


class SVDStyledGraph(StyledGraph):
    category = models.ForeignKey(GraphCategory, null=True)
    
    def __init__(self, *args, **kwargs):
        super(SVDStyledGraph, self).__init__(*args, **kwargs)
        self.sample_colors = {}

    def _get_hashcode_fields(self):
        fields = super(SVDStyledGraph,self)._get_hashcode_fields()
        
        if self.category:
            (sample_groups, group_colors) = self.category.get_sample_groups_and_colors()
            category_fields =  map(str, sample_groups.iteritems()) + map(str, group_colors.iteritems()) 
        else:
            category_fields = ['']
            
        return fields + category_fields
    
    def set_from_graph(self, serialisable_graph):
        super(SVDStyledGraph, self).set_from_graph(serialisable_graph)
        
        self.sample_colors = {s : self.color for s in serialisable_graph.sample_details}

    def set_graph_values(self, serialisable_graph):
        print "SVDGraph.set_graph_values"
        super(SVDStyledGraph, self).set_graph_values(serialisable_graph)

        if self.category:
            (sample_groups, group_colors) = self.category.get_sample_groups_and_colors()
    
            serialisable_graph.set_groups_and_colors(sample_groups, group_colors)


    def save(self, *args, **kwargs):
        super(SVDStyledGraph, self).save(*args, **kwargs)
        
        if self.sample_colors:
            for (sample_name, color) in self.sample_colors.iteritems():
                graph_sample = GraphSample(styled_graph=self,
                                           name=sample_name,
                                           color=color)
                graph_sample.save()

    def get_editor_template(self):
        return 'bgs/graph_editors/svd_graph_editor.html'


def auto_combo_dataframe(project_id, result_type):
    qs = CSVResult.get_for_project_and_result_type(project_id, result_type)
    dataframes = []
    for csv_result in qs:
        csv_result_df = csv_result.get_dataframe()
        dataframes.append(csv_result_df)
        
    df = pd.concat(dataframes, axis=1)
    df = df.fillna(0)
    return df

