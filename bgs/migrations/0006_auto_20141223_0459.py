# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0005_auto_20141222_0753'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileprocessingtask',
            name='error_message',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fileprocessingtask',
            name='name',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='processingresult',
            name='name',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='styledgraph',
            name='title',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='uploadedfile',
            name='uploaded_file',
            field=models.FileField(max_length=256, upload_to=b'uploads'),
            preserve_default=True,
        ),
    ]
