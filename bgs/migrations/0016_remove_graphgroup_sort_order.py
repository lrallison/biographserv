# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0015_auto_20150108_0157'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='graphgroup',
            name='sort_order',
        ),
    ]
