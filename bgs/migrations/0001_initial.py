# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pixelfields_smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AnnotationGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Build',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('annotation_group', models.ForeignKey(to='bgs.AnnotationGroup')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FileProcessingTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('status', models.CharField(max_length=1, choices=[(b'C', b'Created'), (b'P', b'Processing'), (b'E', b'Error'), (b'S', b'Success')])),
                ('error_message', models.CharField(max_length=500)),
                ('created_date', models.DateTimeField()),
                ('start_date', models.DateTimeField(null=True)),
                ('end_date', models.DateTimeField(null=True)),
                ('items_processed', models.IntegerField(default=0)),
                ('celery_task', models.CharField(max_length=36, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organism',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('species_name', models.CharField(max_length=256)),
                ('common_name', models.CharField(max_length=256)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcessingResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Graph',
            fields=[
                ('processingresult_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bgs.ProcessingResult')),
                ('serializable_graph_blob', models.BinaryField()),
            ],
            options={
            },
            bases=('bgs.processingresult',),
        ),
        migrations.CreateModel(
            name='CSVResult',
            fields=[
                ('processingresult_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bgs.ProcessingResult')),
                ('file_name', models.TextField()),
            ],
            options={
            },
            bases=('bgs.processingresult',),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('uuid', models.CharField(max_length=32, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('email', models.CharField(max_length=256)),
                ('build', pixelfields_smart_selects.db_fields.ChainedForeignKey(to='bgs.Build', null=True)),
                ('organism', models.ForeignKey(to='bgs.Organism', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StyledGraph',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('color', models.CharField(default=b'#ff0000', max_length=7)),
                ('hash_code', models.CharField(max_length=40)),
                ('graph', models.ForeignKey(to='bgs.Graph')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UploadedFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('uploaded_file', models.FileField(upload_to=b'uploads')),
                ('file_type', models.CharField(max_length=1, choices=[(b'B', b'BED'), (b'V', b'VCF'), (b'C', b'CuffDiff'), (b'E', b'Expression')])),
                ('date', models.DateTimeField()),
                ('project', models.ForeignKey(to='bgs.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='processingresult',
            name='file_processing_task',
            field=models.ForeignKey(to='bgs.FileProcessingTask'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fileprocessingtask',
            name='uploaded_file',
            field=models.ForeignKey(to='bgs.UploadedFile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='build',
            name='organism',
            field=models.ForeignKey(to='bgs.Organism'),
            preserve_default=True,
        ),
    ]
