# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def insert_references(apps, schema_editor):
    Organism = apps.get_model("bgs", "Organism")
    AnnotationGroup = apps.get_model("bgs", "AnnotationGroup")
    Build = apps.get_model("bgs", "Build")

    (ucsc, _) = AnnotationGroup.objects.get_or_create(name="UCSC")
    (ensembl, _) = AnnotationGroup.objects.get_or_create(name="Ensembl")
    (ncbi, _) = AnnotationGroup.objects.get_or_create(name="NCBI")

    (human, _) = Organism.objects.get_or_create(species_name="Homo_sapiens",
                                                common_name='Human')

    Build.objects.get_or_create(organism=human,
                                annotation_group=ucsc,
                                name='hg18')

    Build.objects.get_or_create(organism=human,
                                annotation_group=ucsc,
                                name='hg19')

    Build.objects.get_or_create(organism=human,
                                annotation_group=ensembl,
                                name='GRCh37')

    (mouse, _) = Organism.objects.get_or_create(species_name="Mus_musculus",
                                                common_name='Mouse')

    Build.objects.get_or_create(organism=mouse,
                                annotation_group=ucsc,
                                name='mm10')

    Build.objects.get_or_create(organism=mouse,
                                annotation_group=ensembl,
                                name='NCBIM37')

    Build.objects.get_or_create(organism=mouse,
                                annotation_group=ensembl,
                                name='GRCm38')

    (arabidopsis, _) = Organism.objects.get_or_create(species_name="Arabidopsis_thaliana",
                                                common_name='Arabidopsis')

    Build.objects.get_or_create(organism=arabidopsis,
                                annotation_group=ensembl,
                                name='TAIR10')

    Build.objects.get_or_create(organism=arabidopsis,
                                annotation_group=ncbi,
                                name='TAIR10')

    (drosophila, _) = Organism.objects.get_or_create(species_name="Drosophila_melanogaster",
                                                common_name='Fruit Fly')

    Build.objects.get_or_create(organism=drosophila,
                                annotation_group=ucsc,
                                name='dm3')


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0001_initial'),
    ]

    operations = [migrations.RunPython(insert_references),
    ]