# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0009_auto_20150102_1018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='styledgraph',
            name='cmap',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='styledgraph',
            name='title',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='styledgraph',
            name='xlabel',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='styledgraph',
            name='ylabel',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
