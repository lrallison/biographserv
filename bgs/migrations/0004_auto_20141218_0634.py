# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0003_auto_20141216_0535'),
    ]

    operations = [
        migrations.AddField(
            model_name='combo',
            name='combo_type',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'R', b'Region Stats'), (b'G', b'Genes'), (b'O', b'Other')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='csvresult',
            name='result_type',
            field=models.CharField(default='O', max_length=1, choices=[(b'R', b'Region Stats'), (b'G', b'Gene Scores'), (b'O', b'Other')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='uploadedfile',
            name='file_type',
            field=models.CharField(max_length=1, choices=[(b'B', b'BED'), (b'V', b'VCF'), (b'C', b'CuffDiff'), (b'S', b'Expression (single sample)'), (b'M', b'Expression (multi-sample)')]),
            preserve_default=True,
        ),
    ]
