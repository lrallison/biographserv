# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0011_auto_20150104_0709'),
    ]

    operations = [
        migrations.AlterField(
            model_name='csvresult',
            name='result_type',
            field=models.CharField(max_length=1, choices=[(b'R', b'Region Stats'), (b'G', b'Bed Gene Scores'), (b'T', b'Read Counts'), (b'O', b'Other'), (b'S', b'Uploaded Single Sample Expression'), (b'M', b'Uploaded Multi Sample Expressions'), (b'C', b'Uploaded CuffDiff')]),
            preserve_default=True,
        ),
    ]
