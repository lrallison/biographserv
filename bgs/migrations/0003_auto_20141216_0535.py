# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0002_auto_20141209_0417'),
    ]

    operations = [
        migrations.CreateModel(
            name='Combo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('file_name', models.TextField()),
                ('project', models.ForeignKey(to='bgs.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ComboColumn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CSVColumn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField()),
                ('column_name', models.TextField()),
                ('csv', models.ForeignKey(to='bgs.CSVResult')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='combocolumn',
            name='csv_column',
            field=models.ForeignKey(to='bgs.CSVColumn'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='combocolumn',
            name='csv_result',
            field=models.ForeignKey(to='bgs.CSVResult'),
            preserve_default=True,
        ),
    ]
