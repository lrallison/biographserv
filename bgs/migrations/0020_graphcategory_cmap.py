# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0019_graphgroup_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphcategory',
            name='cmap',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
