# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0017_auto_20150111_0333'),
    ]

    operations = [
        migrations.AddField(
            model_name='svdstyledgraph',
            name='category',
            field=models.ForeignKey(to='bgs.GraphCategory', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='graphsamplegrouprelationship',
            name='graph_group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='bgs.GraphGroup', null=True),
            preserve_default=True,
        ),
    ]
