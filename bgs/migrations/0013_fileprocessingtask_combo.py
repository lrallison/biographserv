# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0012_auto_20150106_0621'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileprocessingtask',
            name='combo',
            field=models.ForeignKey(to='bgs.Combo', null=True),
            preserve_default=True,
        ),
    ]
