# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0018_auto_20150111_0518'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphgroup',
            name='color',
            field=models.CharField(max_length=7, null=True),
            preserve_default=True,
        ),
    ]
