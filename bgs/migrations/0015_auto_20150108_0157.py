# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bgs', '0014_auto_20150107_0416'),
    ]

    operations = [
        migrations.CreateModel(
            name='GraphCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GraphGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('sort_order', models.IntegerField()),
                ('graph_category', models.ForeignKey(to='bgs.GraphCategory')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GraphSample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('color', models.CharField(max_length=7, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GraphSampleGroupRelationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('graph_group', models.ForeignKey(to='bgs.GraphGroup')),
                ('graph_sample', models.ForeignKey(to='bgs.GraphSample')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SVDStyledGraph',
            fields=[
                ('styledgraph_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='bgs.StyledGraph')),
            ],
            options={
            },
            bases=('bgs.styledgraph',),
        ),
        migrations.AddField(
            model_name='graphsample',
            name='styled_graph',
            field=models.ForeignKey(to='bgs.StyledGraph'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='graphcategory',
            name='styled_graph',
            field=models.ForeignKey(to='bgs.StyledGraph'),
            preserve_default=True,
        ),
    ]
