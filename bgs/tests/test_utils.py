from datetime import datetime
from django.core.files.uploadedfile import SimpleUploadedFile

from bgs import models
from bgs.models import FileProcessingTask

def create_file_processing_task(file_name, file_type):
    upload_file = open(file_name)
    uf = SimpleUploadedFile(upload_file.name, upload_file.read())

    project = models.Project()
    project.save()
    uploaded_file = models.UploadedFile(project=project,
                                        name=file_name,
                                        uploaded_file=uf,
                                        file_type=file_type,
                                        date=datetime.now())
    uploaded_file.save()

    file_processing_task = FileProcessingTask(name='FileProcessingTask',
                                              uploaded_file=uploaded_file,
                                              status=models.ProcessingStatus.CREATED,
                                              created_date=datetime.now())
    file_processing_task.save()
    return file_processing_task