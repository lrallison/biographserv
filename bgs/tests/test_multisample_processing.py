from django.test import TestCase
import os

from bgs.models import UploadedFileTypes
from bgs.tasks.expression import expression_processor
from bgs.tests.test_utils import create_file_processing_task


# Create your tests here.
class TestSVDGraph(TestCase):
    TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data", "multi_expression")

    def data(self, file_name):
        return os.path.join(self.TEST_DATA_DIR, file_name)

    def test_svd_graph(self):
        multi_exp = self.data("sample_multi_expression.random.csv")

        file_processing_task = create_file_processing_task(multi_exp, UploadedFileTypes.MULTI_SAMPLE_EXPRESSION)

        task_runner = expression_processor.MultiSampleExpressionProcessor()
        print "running processor"
        result = task_runner.apply((file_processing_task.pk))

        print "done with processor"
        print result


