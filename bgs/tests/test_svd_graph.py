from collections import OrderedDict
from django.test import TestCase
import os
import pickle

from bgs.models import load_multi_column_df, UploadedFileTypes
from bgs.tasks.expression.svd_processor import SVDProcessor
from bgs.tests.test_utils import create_file_processing_task
from bgs.utils import stats_utils, matplotlib_utils
from bgs.utils.file_utils import mk_path


class TestSVDGraph(TestCase):
    TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data", "multi_expression")
    TEST_OUTPUT_DIR = os.path.join(os.path.dirname(__file__), "test_output")

    def data(self, file_name):
        return os.path.join(self.TEST_DATA_DIR, file_name)

    def output(self, file_name):
        mk_path(self.TEST_OUTPUT_DIR)
        return os.path.join(self.TEST_OUTPUT_DIR, file_name)

    def test_svd_graph(self):
        multi_exp = self.data("sample_multi_expression.random.csv")
        df = load_multi_column_df(multi_exp)
        normalised_df = stats_utils.dataframe_counts_to_rpm(df)

        file_processing_task = create_file_processing_task(multi_exp, UploadedFileTypes.MULTI_SAMPLE_EXPRESSION)

        #colors = matplotlib_utils.get_colors_from_color_map(len(df.columns), 'jet', False)
#        sample_colors = {}
#        for (column, color) in zip(df.columns, colors):
#            sample_colors[column] = color

        sample_groups = OrderedDict()
        for (i, col) in enumerate(df.columns):
            group = "group_%d" % (i / 3)
            sample_groups[col] = group 
             
        groups = set(sample_groups.values())

        #brewer = brewer2mpl.get_map('PuOr', 'Diverging', len(groups))
        #colors = brewer.mpl_color
        colors = matplotlib_utils.get_colors_from_color_map(len(groups), 'jet', False)
        
        # TODO: do something about too white (ie >600?)

        group_colors = OrderedDict()
        for (i, g) in enumerate(groups):
            group_colors[g] = colors[i]

        processor = SVDProcessor()
        processor.process(normalised_df)
        results = processor.get_results()
        svd_graph = results[1]
        svd_graph.file_processing_task = file_processing_task
        svd_graph.save()

        obj = pickle.loads(svd_graph.serializable_graph_blob)
        obj.set_groups_and_colors(sample_groups, group_colors)
        #obj.set_sample_colors(sample_colors)
        graph_image = self.output("svd_plot.png")
        obj.save(graph_image, 'png')

        obj.remove_legend()
        graph_image = self.output("svd_plot_no_legend.png")
        obj.save(graph_image, 'png')

        
#        svd_graph.serializable_graph_blob = pickle.dumps(obj)
#        svd_graph.save()
        
#        styled_graph = StyledGraph.objects.get(graph=svd_graph)
#        print "styled_graph = %s" % styled_graph
#        graph_image = self.output("svd_plot.png")
#        styled_graph.write_image(graph_image)