'''
Created on 25/11/2014

@author: dlawrence
'''

from collections import defaultdict
from datetime import datetime

from bgs import models
from bgs.models import FileProcessingTask, UploadedFileTypes
from bgs.tasks import bed_file_processor
from bgs.tasks import cuffdiff_processor
from bgs.tasks.expression import expression_processor
from bgs.tasks.vcf_processor import vcf_processor


TASK_RUNNERS = {UploadedFileTypes.BED : [bed_file_processor.BedFileProcessor, bed_file_processor.ReferenceBedFileProcessor],
                UploadedFileTypes.CUFFDIFF : [cuffdiff_processor.CuffdiffProcessor],
                UploadedFileTypes.SINGLE_SAMPLE_EXPRESSION : [expression_processor.SingleSampleExpressionProcessor],
                UploadedFileTypes.MULTI_SAMPLE_EXPRESSION : [expression_processor.MultiSampleExpressionProcessor],
                UploadedFileTypes.VCF : [vcf_processor.VCFProcessor],}

def process_uploaded_file(uploaded_file):
    task_runners = TASK_RUNNERS.get(uploaded_file.file_type)
    if not task_runners:
        msg = "Can't find task to process type %s" % uploaded_file.file_type
        raise ValueError(msg)
    
    for task_runner in task_runners:
        file_processing_task = FileProcessingTask(name=task_runner.name,
                                                  uploaded_file=uploaded_file,
                                                  status=models.ProcessingStatus.CREATED,
                                                  created_date = datetime.now())

        file_processing_task.save()

        task = task_runner()
        run_task(task, file_processing_task)

def run_task(task, file_processing_task):
    result = task.apply_async((file_processing_task.pk,))
    # Do as an update as other jobs may be modifying object
    FileProcessingTask.objects.filter(id=file_processing_task.pk).update(celery_task=result.id)


def reference_changed(project):
    for uploaded_file in project.uploadedfile_set.filter(file_type__in=models.UploadedFileTypes.REQUIRES_REFERENCE):
        uploaded_file.fileprocessingtask_set.all().delete()
        process_uploaded_file(uploaded_file)

def reprocess_uploaded_files(uploaded_files_qs):
    for uploaded_file in uploaded_files_qs:
        uploaded_file.fileprocessingtask_set.all().delete()
        process_uploaded_file(uploaded_file)

def genomic_regions_changed(project):
    # TODO: This is wasteful, should only delete what needs to be done...
    uploaded_files_qs = project.uploadedfile_set.all()
    uploaded_files_qs = uploaded_files_qs.filter(file_type__in=models.UploadedFileTypes.REQUIRES_GENOMIC_RANGES)
    reprocess_uploaded_files(uploaded_files_qs)

def reprocess_all_uploaded_files():
    reprocess_uploaded_files(models.UploadedFile.objects.all())

def get_csv_result_types(file_type):
    csv_result_types = set()
    task_runners = TASK_RUNNERS.get(file_type)
    if task_runners:
        for task_runner in task_runners:
            task = task_runner()
            csv_result_types.update(task.get_csv_result_types(file_type))
    return csv_result_types

def get_file_types_csv_result_types_lookup():
    lookup = {}
    for (file_type, _) in UploadedFileTypes.UPLOADED_FILES_CHOICE:
        lookup[file_type] = list(get_csv_result_types(file_type))
        
    return lookup

def get_uploaded_files_producing_result_type(project, result_type):
    lookup = get_file_types_csv_result_types_lookup()
    reverse_lookup = defaultdict(set)
    for (k, values) in lookup.iteritems():
        for v in values:
            reverse_lookup[v].add(k)
    
    file_types = reverse_lookup[result_type]
    return models.UploadedFile.objects.filter(project=project, file_type__in=file_types)

def create_combo_file_processing_task(combo):
    file_processing_task = FileProcessingTask(name='%s FileProcessingTask' % str(combo),
                                              combo=combo,
                                              status=models.ProcessingStatus.CREATED,
                                              created_date=datetime.now())
    file_processing_task.save()
    return file_processing_task


def process_combo(combo):
    # Delete previous data
    FileProcessingTask.objects.filter(combo=combo).delete()
    print "process_combo: %s" % combo

    if combo.is_multi_expression():
        print "Running as multi Combo!"
        task = expression_processor.ComboExpressionProcessor()
        file_processing_task = create_combo_file_processing_task(combo)
        run_task(task, file_processing_task)

        

