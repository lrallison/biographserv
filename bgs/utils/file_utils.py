'''
Created on 01/02/2014

@author: dlawrence
'''

import os


def file_or_file_name(f, mode=None):
    if isinstance(f, basestring): # Works on unicode
        if 'w' in mode: # Create path if writing
            mk_path_for_file(f)
            
        return open(f, mode)
    elif isinstance(f, file):
        return f # Already a File object
    else:
        raise ValueError("'%s' (%s) not a file or string" % (f, type(f)))

def mk_path(path, mode=0775):
    if path and not os.path.exists(path):
        os.makedirs(path, mode)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))

def name_from_file_name(file_name):
    '''Gets file name without extension or directory'''
    return os.path.splitext(os.path.basename(file_name))[0]

def file_to_hash(f, sep=None):
    data = {}
    for line in file_or_file_name(f):
        line = line.rstrip()
        key = line
        value = None
        if sep:
            items = line.split(sep)
            if len(items) >= 2:
                (key, value) = items[0:2]
        data[key] = value
    return data

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename

def file_name_insert(insert, file_name):
    ''' Return "insert" just before the file extension '''
    (file_path, extension) = os.path.splitext(file_name)
    out_filename = "%s.%s%s" % (file_path, insert, extension)
    return out_filename

def get_extension(filename):
    (_, ext) = os.path.splitext(filename)
    return ext[1:]
