'''
Created on 27/06/2013

@author: katherine
'''

from matplotlib import cm
from matplotlib.ticker import Formatter, Locator
import matplotlib.colors as colors
import numpy as np


class MultiplyingLabelFormatter(Formatter):
    '''This is passed to matplotlib plots and is called on every labelling of a tick label'''
    def __init__(self, multiplier):
        self.multiplier = multiplier
        
    def __call__(self, x, pos=None):
        x = int(x)
        return "%d" % (x * self.multiplier)

class ToKiloBaseLabelFormatter(Formatter):
    '''This is passed to matplotlib plots and is called on every labelling of a tick label
    Formats values in bp in kb. E.g. 100 becomes 0.1kb
    kwargs:     show_plus (boolean, default = False): Print positive values with +, e.g. +1kb
                zero-label : What to label 0kb as
                tss (boolean, default=False): Print 'TSS' instead of 0kb'''
    def __init__(self, offset=0, **kwargs):
        self.offset = offset
        self.zero_label = kwargs.get('zero_label')
        if kwargs.get('tss'):
            self.zero_label = 'TSS'
        self.show_plus = kwargs.get('show_plus', False)
        
    def __call__(self, label_in_bp, pos=None):
        label_in_bp -= self.offset
        label_in_kb = label_in_bp / 1000.0
        if self.zero_label and label_in_kb == 0:
            return self.zero_label
        if self.show_plus:
            if label_in_kb > 0:
                return "+%gkb" % label_in_kb
        return "%gkb" % label_in_kb


class OffsetLocator(Locator):
    stride_sizes = [0.5, 1, 2, 5, 10, 20, 50, 100, 500, 1000000]
    
    def __init__(self, offset, num_ticks=6):
        self.offset = offset
        self.num_ticks = 6
    
    def __call__(self):
        'Return the locations of the ticks'
        vmin, vmax = self.axis.get_view_interval()
        return self.tick_values(vmin, vmax)

    def tick_values(self, vmin, vmax):
        size = vmax - vmin
        size_kb = size / 1000
        
        approx_stride = size_kb / self.num_ticks
        stride = -1
        for stride in reversed(self.stride_sizes):
            if stride < approx_stride:
                break
        
        relative_offset = self.offset - vmin
        start = relative_offset % (stride*1000)
        
        ticks = []
        i = vmin + start
        while i <= vmax:
            ticks.append(i)
            i += stride * 1000
            
        return np.array(ticks)
    


def get_colors_from_color_map(num_colors, color_map_string, return_reversed):
    '''
    color_map_string: e.g. Oranges, options shown here: http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps
    return_reversed (boolean): whether to return colors in the reverse order
    Returns array of normalised RGB values'''
    color_indices = range(num_colors)
    cmap = cm.get_cmap(color_map_string)
    if return_reversed:
        color_indices.reverse() #make this dark -> light
        c_norm  = colors.Normalize(vmin=0, vmax=color_indices[0])
    else:
        c_norm  = colors.Normalize(vmin=0, vmax=color_indices[-1])
        
    scalar_map = cm.ScalarMappable(norm=c_norm, cmap=cmap)
    colors_array = [scalar_map.to_rgba(ci) for ci in color_indices]
    return colors_array

def register_and_get_colormap(name, list_of_colors):
    '''list_of_colors= list of colors in hex format
    Returns cmap'''
    colmap = colors.LinearSegmentedColormap.from_list(name, list_of_colors)
    cm.register_cmap(cmap=colmap)
    return cm.get_cmap(name)


def arrowplot(axes, x, y, narrs=30, dspace=0.5, direc='pos', \
                          hl=0.3, hw=6, **kwargs): 
    ''' narrs  :  Number of arrows that will be drawn along the curve

        direc  :  can be 'pos' or 'neg' to select direction of the arrows

        hl     :  length of the arrow head 

        hw     :  width of the arrow head
        
        Extra **kwargs are passed to plot()
        
        This is based off:
        
        http://stackoverflow.com/a/19102353 by Pedro M Duarte
    '''

    c = kwargs.get("color", 'black')

    # r is the distance spanned between pairs of points
    r = [0]
    for i in range(1,len(x)):
        dx = x[i]-x[i-1] 
        dy = y[i]-y[i-1] 
        r.append(np.sqrt(dx*dx+dy*dy))
    r = np.array(r)

    # rtot is a cumulative sum of r, it's used to save time
    rtot = []
    for i in range(len(r)):
        rtot.append(r[0:i].sum())
    rtot.append(r.sum())

    # based on narrs set the arrow spacing
    aspace = r.sum() / narrs

    arrowData = [] # will hold tuples of x,y,theta for each arrow
    arrowPos = aspace

    ndrawn = 0
    rcount = 1 
    while arrowPos < r.sum() and ndrawn < narrs:
        x1,x2 = x[rcount-1],x[rcount]
        y1,y2 = y[rcount-1],y[rcount]
        da = arrowPos-rtot[rcount]
        theta = np.arctan2((x2-x1),(y2-y1))
        ax = np.sin(theta)*da+x1
        ay = np.cos(theta)*da+y1
        arrowData.append((ax,ay,theta))
        ndrawn += 1
        arrowPos+=aspace
        while arrowPos > rtot[rcount+1]: 
            rcount+=1
            if arrowPos > rtot[-1]:
                break

    # could be done in above block if you want
    for ax,ay,theta in arrowData:
        # use aspace as a guide for size and length of things
        # scaling factors were chosen by experimenting a bit

        dx0 = np.sin(theta)*hl/2. + ax
        dy0 = np.cos(theta)*hl/2. + ay
        dx1 = -1.*np.sin(theta)*hl/2. + ax
        dy1 = -1.*np.cos(theta)*hl/2. + ay

        if direc is 'pos' :
            ax0 = dx0 
            ay0 = dy0
            ax1 = dx1
            ay1 = dy1 
        else:
            ax0 = dx1 
            ay0 = dy1
            ax1 = dx0
            ay1 = dy0 

        axes.annotate('', xy=(ax0, ay0), xycoords='data',
                xytext=(ax1, ay1), textcoords='data',
                arrowprops=dict( headwidth=hw, frac=1., ec=c, fc=c))


    axes.plot(x,y, **kwargs)

