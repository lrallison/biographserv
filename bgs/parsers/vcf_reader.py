import vcf

class PatchedVCFReader(vcf.Reader):
    # Strict whitespace to handle spaces in samples 
    def __init__(self, *args, **kwargs):
        kwargs["strict_whitespace"] = kwargs.get("strict_whitespace", True) # should have been the default!
        super(PatchedVCFReader, self).__init__(*args, **kwargs)

    # Our VCF files have consScoreGERP=NA which violates the spec - convert to '.'
    def _map(self, func, iterable, bad='.'):
        if func == float:
            iterable = (x if x != 'NA' else '.' for x in iterable)
        return super(PatchedVCFReader, self)._map(func, iterable, bad)