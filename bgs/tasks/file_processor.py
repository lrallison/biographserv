from celery.app.task import Task
from datetime import datetime
import logging

from bgs.models import ProcessingStatus, FileProcessingTask, CSVResult, CSVResultType, \
    get_processing_dir


INITIAL_PROGRESS_STATUS = "Processing"

class AbstractFileProcessor(Task):
    abstract = True

    def start(self, file_processing_task):
        file_processing_task.status = ProcessingStatus.PROCESSING
        file_processing_task.start_date = datetime.now()
        file_processing_task.save()

    def success(self, file_processing_task, items_processed):
        file_processing_task.status = ProcessingStatus.SUCCESS
        #file_processing_task.items_processed = items_processed
        file_processing_task.end_date = datetime.now()
        file_processing_task.save()
    
    def error(self, file_processing_task):
        file_processing_task.status = ProcessingStatus.ERROR
        file_processing_task.end_date = datetime.now()
        file_processing_task.save()

    def process_items(self, file_processing_task):
        ''' returns (items_processed, results) '''
        pass

    def get_csv_result_types(self, file_type):
        return set()

    def run(self, file_processing_task_id):
        logger = logging.getLogger(__name__)

        file_processing_task = FileProcessingTask.objects.get(pk=file_processing_task_id)
        # Call this first, so that celery makes the processing directory (before reference_server)
        project = file_processing_task.get_project()
        processing_dir = get_processing_dir(project.pk)
        print "Processing dir: %s" % processing_dir

        try:
            self.start(file_processing_task)

            (items_processed, results) = self.process_items(file_processing_task)

            for result in results:
                result.file_processing_task = file_processing_task
                result.save()
                print "result: %s" % result

            self.success(file_processing_task, items_processed)
        except Exception as e:
            logger.error(e)
            self.error(file_processing_task)
            

class FileProcessor(AbstractFileProcessor):
    abstract = True
    
    def get_file_reader(self, uploaded_file):
        pass

    def get_processors_for_task(self, file_processing_task):
        kwargs = {'project_id' : file_processing_task.get_project().pk,
                  'base_name' : file_processing_task.get_base_name(),}
        
        if file_processing_task.uses_reference():
            reference = file_processing_task.get_reference()
            kwargs["reference"] = reference

        return self.get_processors(**kwargs)

    def get_csv_result_types(self, file_type):
        csv_result_types = set()
        if file_type in CSVResultType.UPLOADED_CSVRESULTS:
            csv_result_types.update(file_type)
        return csv_result_types
    
    def get_processors(self, **kwargs):
        return []

    def feed_processors(self, reader, processors):
        items_processed = 0
        for chunk in reader:
            for processor in processors:
                processor.process(chunk)
            
            items_processed += 1
        
        return items_processed

    def get_unprocessed_results(self, uploaded_file):
        results = []
        if uploaded_file and uploaded_file.file_type in CSVResultType.UPLOADED_CSVRESULTS:
            results = [CSVResult(name="", #"Uploaded File: %s" % uploaded_file.name,
                                 file_name=uploaded_file.uploaded_file.path,
                                 result_type=uploaded_file.file_type)]
        return results


    def process_items(self, file_processing_task):
        ''' returns (items_processed, results) '''

        print "Get reader"            
        self.file_processing_task = file_processing_task
        uploaded_file = file_processing_task.uploaded_file
        reader = self.get_file_reader(uploaded_file)

        results = []
        results.extend(self.get_unprocessed_results(uploaded_file))
        processors = self.get_processors_for_task(file_processing_task)

        if processors:
            for processor in processors:
                processor.set_reader(reader)

            print "Starting read"
            items_processed = self.feed_processors(reader, processors)

            print "Finished processing"
            for processor in processors:
                results.extend(processor.get_results())

        return (items_processed, results)
