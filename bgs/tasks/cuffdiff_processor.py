'''
Created on 25/11/2014

@author: dlawrence
'''

import pandas as pd
from bgs.graphs.cuffdiff.volcano_graph import VolcanoGraph
from bgs.tasks.file_processor import FileProcessor


class CuffdiffProcessor(FileProcessor):
    def get_file_reader(self, uploaded_file):
        return pd.DataFrame.from_csv(uploaded_file.uploaded_file.path, sep=None)

    def get_processors(self, **kwargs):
        return [VolcanoGraph()]

    def feed_processors(self, df, processors):
        for processor in processors:
            processor.process(df)

        return len(df)

