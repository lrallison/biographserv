import numpy as np
import pandas as pd
from bgs.graphs.series_boxplot_graph import SeriesBoxplotGraph, \
    SeriesMultiValueBoxplotGraph
from bgs.graphs.bar_graph import SeriesValuesBarGraph


class VCFPandasProcessor(object):
    def __init__(self, *args, **kwargs):
        super(VCFPandasProcessor, self).__init__(*args, **kwargs)
        self.records = []
        
    ''' Converts a VCF into a pandas dataframe '''
    def set_reader(self, reader):
        self.reader = reader
        
    def process(self, v):
        for (alt_id, _) in enumerate(v.ALT):
            record = {"QUALITY" : v.QUAL}
            for (key, value) in v.INFO.iteritems():
                info = self.reader.infos[key]
                if info.num == 0: # Flag
                    value = True
                elif info.num == -1:
                    value = value[alt_id]
                elif info.num == -2: # Dont' handle genotype
                    continue
                record[key] = value
            self.records.append(record)

    def get_results(self):
        print "get_results!"
        df = pd.DataFrame.from_records(self.records)
        #df.to_csv("pandas_vcf.csv")

        results = []
        for column in df.columns:
            info = self.reader.infos.get(column)
            series = df[column]

            if info and info.num > 1:
                if info.type in ['Float', 'Integer']:
                    graph = SeriesMultiValueBoxplotGraph(series, info.num, column)
                else:
                    print "No idea how to handle %s" % info
                    continue
            else:
                if info and info.num == 0: # Flag
                    series = series.fillna(False)
                    
                dtype = df.dtypes[column]
                if dtype in (np.float64, np.int):
                    graph = SeriesBoxplotGraph(series, column)
                else:
                    graph = SeriesValuesBarGraph(series, column)

            results.extend(graph.get_results())
        
        return results
