'''
Created on 25/11/2014

@author: dlawrence
'''

from bgs.parsers import vcf_reader
from bgs.tasks.file_processor import FileProcessor
from bgs.tasks.vcf_processor.vcf_pandas_processor import VCFPandasProcessor


class VCFProcessor(FileProcessor):
    def get_file_reader(self, uploaded_file):
        f = open(uploaded_file.uploaded_file.path)
        return vcf_reader.PatchedVCFReader(f)

    def get_processors(self, **kwargs):
        return [VCFPandasProcessor()]



    