import numpy as np
import pandas as pd
from bgs.graphs.expression.scree_graph import ScreeGraph
from bgs.graphs.expression.svd_graph import SVDGraph
from bgs.utils.stats_utils import rename_common_suffix_columns


no_rename_index = False
low_expression_cutoff = 3

class SVDProcessor(object):
    normalized_input = True
    scree_graph = ScreeGraph()
    svd_graph = SVDGraph()

    def set_reader(self, df):
        pass
    
    def process(self, df):
        if not no_rename_index:
            df = rename_common_suffix_columns(df, df.columns)
        
        # Only use genes where at least one sample has a decent (log > 3) expression
        log_df = np.log(df + 0.01) # Add small amount so not inf log
        min_mask = log_df.max(axis=1) > low_expression_cutoff
        log_df = log_df[min_mask]

        U, s, Vt = np.linalg.svd(log_df, full_matrices=False)
        s = s[:15] # Don't want too much
        
        vt_df = pd.DataFrame(data=Vt, columns=log_df.columns)
        self.svd_graph.process(vt_df)
        self.scree_graph.process(s)


    def get_results(self):
        results = []
        results.extend(self.scree_graph.get_results())
        results.extend(self.svd_graph.get_results())
        return results
