'''
Created on 07/12/2014

@author: dlawrence
'''
import os

from bgs.graphs.bar_graph import BarGraphProcessor
from bgs.models import CSVResult, CSVResultType, get_processing_dir

class ReadCountProcessor(object):
    normalized_input = False

    def __init__(self, **kwargs):
        self.base_name = kwargs["base_name"]
        self.project_id = kwargs["project_id"]
    
    def set_reader(self, df):
        pass

    def get_csv_result_types(self, file_type):
        return set([CSVResultType.READ_COUNTS,])
    
    def process(self, df):
        counts_df = df.sum()
        working_dir = get_processing_dir(self.project_id)
        counts_csv_filename = os.path.join(working_dir, '%s_read_counts.csv' % self.base_name)
        counts_df.to_csv(counts_csv_filename)
        self.counts_csv_result = CSVResult(name='Read Counts',
                                                result_type=CSVResultType.READ_COUNTS,
                                                file_name=counts_csv_filename)
        

        self.counts_graph = BarGraphProcessor(counts_df, "Read Counts")

    def get_results(self):
        results = [self.counts_csv_result]
        results.extend(self.counts_graph.get_results())
        return results
