'''
Created on 25/11/2014

@author: dlawrence
'''

from bgs.models import load_single_column_df, load_multi_column_df
from bgs.tasks.expression.correlation_processor import CorrelationProcessor
from bgs.tasks.expression.expression_boxplot_processor import ExpressionBoxplotProcessor
from bgs.tasks.expression.read_count_processor import ReadCountProcessor
from bgs.tasks.expression.svd_processor import SVDProcessor
from bgs.tasks.file_processor import FileProcessor
from bgs.utils.stats_utils import dataframe_counts_to_rpm


class SingleSampleExpressionProcessor(FileProcessor):
    def get_file_reader(self, uploaded_file):
        return load_single_column_df(uploaded_file.uploaded_file.path)

    def get_processors(self, **kwargs):
        return [ExpressionBoxplotProcessor()]

    def feed_processors(self, df, processors):
        for processor in processors:
            processor.process(df)

        return len(df)

class MultiSampleExpressionProcessor(FileProcessor):
    def get_file_reader(self, uploaded_file):
        return load_multi_column_df(uploaded_file.uploaded_file.path)

    def get_processors(self, **kwargs):
        return [SVDProcessor(), CorrelationProcessor(**kwargs), ReadCountProcessor(**kwargs)]

    def feed_processors(self, df, processors):
        raw_processors = []
        normalized_processors = []

        for processor in processors:
            if processor.normalized_input:
                normalized_processors.append(processor)
            else:
                raw_processors.append(processor)

        for processor in raw_processors:
            processor.process(df)

        rpm_df = dataframe_counts_to_rpm(df)
            
        if normalized_processors:
            for processor in normalized_processors:
                processor.process(rpm_df)
            
        return len(df)

class ComboExpressionProcessor(MultiSampleExpressionProcessor):
    def get_file_reader(self, uploaded_file):
        combo = self.file_processing_task.combo
        return combo.get_dataframe()
