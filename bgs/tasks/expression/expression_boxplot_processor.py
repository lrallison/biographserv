'''
Created on 18/12/2014

@author: dlawrence
'''
from bgs.graphs.series_boxplot_graph import SeriesBoxplotGraph


class ExpressionBoxplotProcessor(object):
    def set_reader(self, df):
        pass
    
    def process(self, df):
        self.df = df

    def get_results(self):
        column_label = "Expression"

        results = []
        for (_, series) in self.df.iteritems():
            graph = SeriesBoxplotGraph(series, column_label)
            results.extend(graph.get_results())
        return results
