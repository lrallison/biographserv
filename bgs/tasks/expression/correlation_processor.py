'''
Created on 07/12/2014

@author: dlawrence
'''
import os

from bgs.graphs.expression.correlation_matrix import CorrelationMatrix
from bgs.models import CSVResult, CSVResultType, get_processing_dir
from bgs.utils.stats_utils import correlation_dataframe

class CorrelationProcessor(object):
    normalized_input = False

    def __init__(self, **kwargs):
        self.base_name = kwargs["base_name"]
        self.project_id = kwargs["project_id"]
    
    def set_reader(self, df):
        pass

    def get_csv_result_types(self, file_type):
        return set([CSVResultType.OTHER,])
    
    def process(self, df):
        correlation_df = correlation_dataframe(df)
        # Sort rows and columns...
        correlation_df = correlation_df.sort_index(axis=0).sort_index(axis=1)
        
        working_dir = get_processing_dir(self.project_id)
        correlation_csv_filename = os.path.join(working_dir, '%s_correlations.csv' % self.base_name)
        correlation_df.to_csv(correlation_csv_filename)
        self.correlation_csv_result = CSVResult(name='Correlations',
                                                result_type=CSVResultType.OTHER,
                                                file_name=correlation_csv_filename)
        
        self.correlation_matrix = CorrelationMatrix()
        self.correlation_matrix.process(correlation_df)

    def get_results(self):
        print "Results..."
        results = []
        results.extend(self.correlation_matrix.get_results())
        results.append(self.correlation_csv_result)
        return results
