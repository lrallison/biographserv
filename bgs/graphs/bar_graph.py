'''
Created on 05/02/2014

@author: dlawrence
'''

import numpy as np
from bgs.graphs.processing_graph import ProcessingGraph

# NOTE: Can't pickle bar graphs, until issue is fixed:
# https://github.com/matplotlib/matplotlib/issues/1719
class BarGraphProcessor(ProcessingGraph):
    def __init__(self, series, label, *args, **kwargs):
        super(BarGraphProcessor, self).__init__(*args, **kwargs)
        self.series = series
        self.title = label

    def get_name(self):
        return self.title
        
    def process(self, series):
        pass
    
    def get_colors_description(self):
        return ["bars"]

    def set_colors(self, colors):
        color = colors[0]
        
        ax = self.axes()
        ax.clear()
        barlist = self.plot_bars(ax)
        for bar in barlist:
            bar.set_color(color)

    def _plot(self, ax):
        self.plot_bars(ax)

    def plot_bars(self, ax):
        ind = np.arange(len(self.series))
        width = 0.4

        barlist = ax.bar(ind, self.series, width=width)
        labels = list(self.series.index)
        rotation = 'vertical'
        ax.set_xticks(ind+width/2.)
        ax.set_xticklabels(labels, rotation=rotation)
        return barlist

class SeriesValuesBarGraph(BarGraphProcessor):
    def __init__(self, series, label, *args, **kwargs):
        value_counts = series.value_counts()        
        super(SeriesValuesBarGraph, self).__init__(value_counts, label, *args, **kwargs)
        
