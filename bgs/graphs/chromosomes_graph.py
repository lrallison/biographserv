'''

Based on example code from Ryan Dale - https://www.biostars.org/p/9922/#9969

Created on 03/02/2014

@author: dlawrence
'''
from matplotlib.collections import BrokenBarHCollection
from biographserv import settings
from bgs.utils.utils import sorted_nicely
from reference_server.genomics import format_chrom


DEFAULT_COLOR_LOOKUP = {
                  'gneg': (1., 1., 1.),
                'gpos25': (.6, .6, .6),
                'gpos50': (.4, .4, .4),
                'gpos75': (.2, .2, .2),
               'gpos100': (0., 0., 0.),
                  'acen': (.8, .4, .4),
                  'gvar': (.8, .8, .8),
                 'stalk': (.9, .9, .9),
}


def ideograms(fn, color_lookup):
    last_chrom = None
    fin = open(fn)
    fin.readline()
    xranges, colors = [], []

    for line in fin:
        chrom, start, stop, _, stain = line.strip().split('\t')
        start = int(start)
        stop = int(stop)
        width = stop - start
        if chrom == last_chrom or (last_chrom is None):
            xranges.append((start, width))
            colors.append(color_lookup[stain])
            last_chrom = chrom
            continue

        yield xranges, colors, last_chrom
        xranges, colors = [], []
        xranges.append((start, width))
        colors.append(color_lookup[stain])
        last_chrom = chrom

    yield xranges, colors, last_chrom

def sorted_ideograms(fn, **kwargs):
    height = kwargs.get('height', 0.9)
    spacing = kwargs.get('spacing', 0.9)
    color_lookup = kwargs.get('color_lookup', DEFAULT_COLOR_LOOKUP)
    
    chromosomes = {}
    ymin = 0
    for xranges, colors, chrom in ideograms(fn, color_lookup):
        chromosomes[chrom] = (xranges, colors, chrom)
    
    for key in reversed(sorted_nicely(chromosomes)):
        (xranges, colors, chrom) = chromosomes[key]
        ymin += height + spacing
        yrange = (ymin, height)
        yield (xranges, yrange, colors, chrom)

def plot_chromosomes(ideogram_file, ax, **kwargs):
    ''' ideogram.txt downloaded from UCSC's table browser '''

    yticks = []
    yticklabels = []
    chrom_ranges = {}
    
    for xranges, yrange, colors, label in sorted_ideograms(ideogram_file, **kwargs):
        coll = BrokenBarHCollection(xranges, yrange, facecolors=colors)
        ax.add_collection(coll)
        center = yrange[0] + yrange[1]/2.
        yticks.append(center)
        yticklabels.append(label)
        key = format_chrom(label, settings.CHROMOSOME_FORMAT_HAS_CHR)
        chrom_ranges[key] = (xranges, yrange)

    ax.axis('tight')
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.set_xticks([])
    return chrom_ranges
