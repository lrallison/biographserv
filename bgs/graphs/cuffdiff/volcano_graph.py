'''
Created on 05/02/2014

@author: dlawrence
'''
from numpy import log10
from bgs.graphs.processing_graph import ProcessingGraph

class VolcanoGraph(ProcessingGraph):
    def __init__(self, *args, **kwargs):
        super(VolcanoGraph, self).__init__(*args, **kwargs)
        self.xlabel = "log2 fold change"
        self.ylabel = "-log10(p-value)"
        self.top_axis = False
        self.right_axis = False
        self.ygrid = True
    
    def process(self, chunk):
        self.df = chunk

    def get_colors_description(self):
        return ["points"]
    
    def set_colors(self, colors):
        col = colors[0]
        self.scatter.set_color(col)

    def get_name(self):
        return "Volcano Plot"

    def _plot(self, ax):
        fold_change = 'log2(fold_change)'
        p_value = "p_value"
        x = self.df[fold_change]
        y = -log10(self.df[p_value])
        self.scatter = ax.scatter(x, y, color='red', alpha=0.5, s=4)
        
        significant = -log10(0.05)
        ax.axhline(y=significant, linestyle='--', color='purple')
        
        ax.set_ylim(0, 16)
        ax.set_xlim(-20, 20)
