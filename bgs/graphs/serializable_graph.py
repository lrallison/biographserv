'''
Created on 01/02/2014

@author: dlawrence
'''
import abc
import matplotlib
matplotlib.use('Agg')
from matplotlib.axes import Axes
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_pdf import FigureCanvasPdf
from matplotlib.backends.backend_ps import FigureCanvasPS
from matplotlib.backends.backend_svg import FigureCanvasSVG
from matplotlib.figure import Figure
import os
import tempfile
from threading import Lock
from time import time

from bgs.models import StyledGraph


class SerializableGraph(object):
    IMAGE_TYPES = {'eps' : 'application/eps',
                   'pdf' : 'application/pdf',
                   'png' : 'image/png',
                   'svg' : 'image/svg',
                   'tif' : 'image/tif'}
    __metaclass__ = abc.ABCMeta

    def __init__(self, **kwargs):
        figure = Figure(dpi=None)
        figure.clear()
        figure.patch.set_facecolor('white')
        self.figure = figure

        self.cmap = None
        self.colors = ["#ff0000"] # Array so you can have multiple, though most of the time only use 1
        self.title = None
        self.xlabel = None
        self.ylabel = None
        self.top_axis = True
        self.bottom_axis = True
        self.left_axis = True
        self.right_axis = True
        self.xgrid = False
        self.ygrid = False

    def plot(self):
        # get_subplot seems to reuse axes, which stuffs us up with threads.
        lock = Lock()
        with lock:
            margins = [0.125, 0.1, 0.8, 0.8]
            ax = Axes(self.figure, margins)
            ax.clear()
            self.figure.add_axes(ax)
            self._plot(ax)
            self._apply_styles(ax)

    @abc.abstractmethod
    def _plot(self, ax):
        return

    def has_colormap(self):
        return False

    def set_colormap(self, cmap):
        return

    def number_of_colors(self):
        return len(self.get_colors_description())

    def get_colors_description(self):
        ''' Return a string per accepted colour '''
        return []

    def set_colors(self, colors):
        return

    def get_styled_graph_class(self):
        return StyledGraph

    def _apply_styles(self, ax):
        start = time()
        print "Apply Styles"

        if self.has_colormap():
            self.set_colormap(self.cmap)

        accepted_colors = self.get_colors_description()
        if accepted_colors:
            if len(accepted_colors) != len(self.colors):
                print "Warning: accepted colours %d != self.colors len %d" % (len(accepted_colors), len(self.colors))
            self.set_colors(self.colors)

        if self.title is not None:
            ax.set_title(self.title)
        
        if self.xlabel is not None:
            ax.set_xlabel(self.xlabel)
        if self.ylabel is not None:
            ax.set_ylabel(self.ylabel)

        ax.spines["top"].set_visible(self.top_axis) 
        ax.spines["bottom"].set_visible(self.bottom_axis) 
        ax.spines["left"].set_visible(self.left_axis) 
        ax.spines["right"].set_visible(self.right_axis) 

        ax.xaxis.grid(self.xgrid)
        ax.yaxis.grid(self.ygrid)
        end = time()
        print "styled_graph.apply_styles: %f" % (end - start)
    

    @classmethod
    def get_content_type(cls, image_type):
        if image_type not in cls.IMAGE_TYPES:
            msg = "Uknown image type '%s' (must be: %s)" % (image_type, ','.join(cls.IMAGE_TYPES))
            raise ValueError(msg)
        else:
            return cls.IMAGE_TYPES[image_type] 

    def pre_save(self):
        self.figure.set_tight_layout(True)

    def axes(self):
        return self.figure.axes[0]

    def save(self, filename_or_obj, image_type):
        method_name = "print_%s" % image_type
        method = getattr(self, method_name)
        return method(filename_or_obj)

    def print_eps(self, filename_or_obj):
        # I get "AttributeError: readable" if I feed in a response or file object, 
        # So need to use string of temp file and manually unlink
        self.pre_save()
        canvas = FigureCanvasPS(self.figure)
        (_, temp_file_name) = tempfile.mkstemp()
        canvas.print_eps(temp_file_name)

        temp_file = open(temp_file_name)
        filename_or_obj.write(temp_file.read())
        temp_file.close()
        os.unlink(temp_file_name)
        
    def print_pdf(self, filename_or_obj):
        self.pre_save()
        canvas = FigureCanvasPdf(self.figure)
        canvas.print_pdf(filename_or_obj)

    def print_png(self, filename_or_obj):
        self.pre_save()
        canvas = FigureCanvasAgg(self.figure)
        canvas.print_png(filename_or_obj)

    def print_svg(self, filename_or_obj):
        self.pre_save()
        canvas = FigureCanvasSVG(self.figure)
        canvas.print_svg(filename_or_obj)

    def print_tif(self, filename_or_obj):
        self.pre_save()
        temp_file = tempfile.TemporaryFile()
        canvas = FigureCanvasAgg(self.figure)
        canvas.print_tif(temp_file)
        temp_file.seek(0)
        filename_or_obj.write(self.axes())
        
