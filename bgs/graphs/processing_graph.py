import abc

from bgs.graphs.serializable_graph import SerializableGraph
from bgs.models import Graph


class ProcessingGraph(SerializableGraph):
    def set_reader(self, df):
        pass

    @abc.abstractmethod
    def process(self, chunk):
        return

    @abc.abstractmethod
    def get_name(self):
        return

    def get_results(self):
        self.plot()
        graph = Graph(name=self.get_name(),
                      serializable_graph=self)
        return [graph]
