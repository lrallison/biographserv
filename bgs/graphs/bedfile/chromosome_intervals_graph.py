from collections import defaultdict
import numpy as np
from bgs.graphs import chromosomes_graph
from bgs.graphs.processing_graph import ProcessingGraph

LINE_WIDTH = 11

class ChromosomeIntervalsGraph(ProcessingGraph):
    def __init__(self, **kwargs):
        reference = kwargs.pop("reference")

        super(ChromosomeIntervalsGraph, self).__init__()

        self.ideogram_file = reference.ideogram
        self.lines = []
        self.genomic_intervals = []

    def process(self, chunk):
        self.genomic_intervals.append(chunk.iv)

    def get_colors_description(self):
        return ["lines"]
    
    def set_colors(self, colors):
        col = colors[0]
        for line in self.lines:
            line.set_color(col)

    def get_chrom_xmin_xmax(self):
        chrom_xmin_xmax = defaultdict(lambda : ([], []))

        for iv in self.genomic_intervals:
            c = chrom_xmin_xmax[iv.chrom]
            c[0].append(iv.start)
            c[1].append(iv.end)
            
        return chrom_xmin_xmax

    def get_name(self):
        return "Chromosome Intervals Graph"

    def _plot(self, ax):
        height = 1
        spacing = 0.4

        chrom_ranges = chromosomes_graph.plot_chromosomes(self.ideogram_file, ax, height=height, spacing=spacing)
        chrom_y = {}
        for chrom, (_, yrange) in chrom_ranges.iteritems():
            y = yrange[0] + yrange[1]/2.0
            chrom_y[chrom] = y
        
        chrom_xmin_xmax = self.get_chrom_xmin_xmax()

        self.lines = []
        for (chrom, (xmin, xmax)) in chrom_xmin_xmax.iteritems():
            y = chrom_y.get(chrom)
            if y is not None:
                y_array = np.empty(len(xmin))
                y_array.fill(y)
                lines = ax.hlines(y_array, xmin, xmax, color='blue', lw=LINE_WIDTH, alpha=0.5)
                self.lines.append(lines) 
            else:
                print "Skipping uknown chrom from genomic intervals file: %s" % chrom

