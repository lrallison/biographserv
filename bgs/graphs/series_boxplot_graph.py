'''
Created on 05/02/2014

@author: dlawrence
'''

import numpy as np

from bgs.graphs.processing_graph import ProcessingGraph

class SeriesBoxplotGraph(ProcessingGraph):
    def __init__(self, series, label, *args, **kwargs):
        super(SeriesBoxplotGraph, self).__init__(*args, **kwargs)
        self.series = series
        self.label = label

    def get_name(self):
        return self.label
        
    def process(self, series):
        pass

    def get_colors_description(self):
        return ["box face"]
    
    def set_colors(self, colors):
        color = colors[0]
        for patch in self.boxplot['boxes']:
            patch.set_facecolor(color)

    def _plot(self, ax):
        # I get: ImportError: No module named _backend_gdk
        # df.boxplot(ax=ax)  

        ok = np.isfinite(self.series*1.01) # Handle almost infinity
        series = self.series[ok]

        self.boxplot = ax.boxplot([series], patch_artist=True)
        ax.set_xticklabels([self.label])


class SeriesMultiValueBoxplotGraph(ProcessingGraph):
    def __init__(self, multi_value_series, number, label, *args, **kwargs):
        super(SeriesMultiValueBoxplotGraph, self).__init__(*args, **kwargs)

        multi_value_series = multi_value_series.dropna()

        series_list = [[] for _ in range(number)]
        for row in multi_value_series:
                for i in range(number):
                    series_list[i].append(row[i])
        
        self.series_list = [np.array(i) for i in series_list]
        self.label = label
        
    def process(self, series):
        pass
    
    def get_colors_description(self):
        return ["box face"]

    def set_colors(self, colors):
        color = colors[0]
        for patch in self.boxplot['boxes']:
            patch.set_facecolor(color)

    def _plot(self, ax):
        # I get: ImportError: No module named _backend_gdk
        # df.boxplot(ax=ax)  

        self.boxplot = ax.boxplot(self.series_list, patch_artist=True)
        ticklabels = ["%s (%d)" % (self.label, i) for i in range(len(self.series_list))]
        ax.set_xticklabels(ticklabels)
    