'''
Created on 29/11/2014

@author: dlawrence
'''

import numpy as np
from bgs.graphs.processing_graph import ProcessingGraph

class ScreeGraph(ProcessingGraph):
    def __init__(self, *args, **kwargs):
        super(ScreeGraph, self).__init__(*args, **kwargs)
        self.xlabel = "Components"
        #If remove square from above, and change this to 'eigenvalue' or 'singular value'. Square only correct for column-mean-centred matrix decomposition (PCA).        
        self.ylabel = "% of Variance" 
        self.title = "Scree Plot"
    
    def process(self, s):
        self.s = s
    
    def get_colors_description(self):
        return ["points"]

    def set_colors(self, colors):
        col = colors[0]
        for p in self.points:
            p.set_color(col)

    def get_name(self):
        return "Scree Plot"

    def _plot(self, ax):
        x = np.arange(1, len(self.s) + 1)
        eigen_values = self.s ** 2 #probably don't need these to be squared, but if you don't square it, don't use '% of variance'
        # absolute values #TODO: check whether it's valid to use squared eigenvalues for SVD (as distinct from PCA). If not, revert to using unsquared eigenvalues
        #y = eigen_values
        # as %
        y = eigen_values / eigen_values.sum() #normalise to be the fraction of the total variance
    
        self.points = ax.plot(x, y, 'ro')
        ax.set_ylim(ymin=0)
        ax.set_xlim(1, len(x))
        ax.set_xticks(range(1, len(self.s) + 1))
