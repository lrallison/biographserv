#!/usr/bin/env python
'''
Created on 10/11/2014

@author: dlawrence
'''

from math import ceil
from matplotlib.patches import Rectangle

from bgs.graphs.processing_graph import ProcessingGraph
from bgs.models import SVDStyledGraph


X_COMPONENT = 0
Y_COMPONENT = 1

class SVDGraph(ProcessingGraph):
    def process(self, vt_df):
        self.vt_df = vt_df
    
    def get_colors_description(self):
        return ["sample names"]

    def set_groups_and_colors(self, sample_groups, group_colors):
        for (sample_name, data) in self.sample_details.iteritems():
            (scatter, annotation) = data
            group_name = sample_groups[sample_name]
            col = group_colors[group_name]
            scatter.set_color(col)
            annotation.set_color(col)

        self.add_legend(group_colors)


    def set_sample_colors(self, sample_colors):
        for (sample_name, data) in self.sample_details.iteritems():
            (scatter, annotation) = data
            col = sample_colors[sample_name]
            scatter.set_color(col)
            annotation.set_color(col)


    def set_colors(self, colors):
        col = colors[0]
        for (_, data) in self.sample_details.iteritems():
            (scatter, annotation) = data
            scatter.set_color(col)
            annotation.set_color(col)

    def get_styled_graph_class(self):
        return SVDStyledGraph

    def get_name(self):
        return "SVD plot"

    def _plot(self, ax):
        self.legend = None
        self.original_position = ax.get_position()

        # We can't draw text directly as that won't space the plot out right
        # So we basically draw invisible dots in the plot (alpha=0) then annotate them
        self.sample_details = {}

        for (sample_name, col) in self.vt_df.iteritems():
            x = col[X_COMPONENT]
            y = col[Y_COMPONENT]
            scatter = ax.scatter(x, y, alpha=0, label=sample_name)
            annotation = ax.annotate(sample_name, (x, y), horizontalalignment='center', verticalalignment='center')

            self.sample_details[sample_name] = (scatter, annotation)

    def remove_legend(self):
        if self.legend:
            self.legend.remove()
            self.legend = None
            ax = self.axes()
            ax.set_position(self.original_position)

    def add_legend(self, sample_colors):
        if self.legend:
            self.remove_legend()
            
        ax = self.axes()

        legend_ratio = 0.1
        ncol = len(sample_colors)
        if ncol > 4:
            legend_ratio = 0.15
            ncol = min(4, int(ceil(ncol / 2.0)))
            print "ncol = %d" % ncol
        
        # Put a legend below current axis
        box = self.original_position
        ax.set_position([box.x0, box.y0 + box.height * legend_ratio,
                         box.width, box.height * (1.0 - legend_ratio)])
    
        # Need to build our own legend as normal one has alpha of 0
        patches = []
        labels = []
        for (name, color) in sample_colors.iteritems():
            labels.append(name)
            patches.append(Rectangle((0, 0), 1, 1, fc=color))
    
        kwargs = {"loc" : 'upper center', "bbox_to_anchor" : (0.5, -0.11)}
        if ncol:
            kwargs["ncol"] = ncol
        self.legend = ax.legend(patches, labels, **kwargs)
