#!/usr/bin/env python
'''
Created on 10/11/2014

@author: dlawrence
'''

from matplotlib.cm import ScalarMappable
import numpy as np
from bgs.graphs.processing_graph import ProcessingGraph

MAX_SIZE_FOR_TEXT_DEFAULT=10

class CorrelationMatrix(ProcessingGraph):
    def process(self, correlation_df):
        self.correlation_df = correlation_df
        self.cmap = 'RdYlGn'
    
    def set_colormap(self, cmap):
        if cmap:
            self.pcolor.set_cmap(cmap)
            
            # Colorbar
            axes = self.figure.axes
            if len(axes) > 1:
                colorbar_axis = axes[-1]
                self.figure.delaxes(colorbar_axis)
                
            mappable = ScalarMappable(cmap=cmap)
            mappable.set_array(self.correlation_df.as_matrix())
            self.figure.colorbar(mappable)

    def has_colormap(self):
        return True

    def get_colors_description(self):
        return [] # None

    def get_name(self):
        return "Correlation Matrix"

    def _plot(self, ax):
        print "Correlation matrix plot()"
        self.pcolor = ax.pcolor(self.correlation_df, cmap=self.cmap)

        columns = self.correlation_df.columns
        index = self.correlation_df.index
        
        ax.tick_params(axis='both', direction='out')
        ax.set_xticks(np.arange(len(columns)) + 0.5)
        ax.set_xticklabels(columns, fontsize=6, rotation='vertical')
        ax.set_yticks(np.arange(len(index)) + 0.5)
        ax.set_yticklabels(index, fontsize=6)
        
        if len(columns) <= MAX_SIZE_FOR_TEXT_DEFAULT:
            for (r, row) in enumerate(self.correlation_df.as_matrix()):
                for (c, cell) in enumerate(row):
                    ax.text(r+0.5, c+0.5, "%.2f" % cell, va='center', ha='center')
