'''
A genetic transcript: ie a particular segment of DNA is copied into RNA by the enzyme RNA polymerase

Created in Reference from a .GTF

5/3/2013 by dlawrence: Removed get_protein() due to annotation/frame shift problems - If you want a protein, use a database

@see sacgf.genomics.reference.Reference

Created on Sep 6, 2012

@author: dlawrence
'''

import HTSeq
from collections import defaultdict
import logging

from bgs.utils.utils import Lazy
from reference_server import genomics


logger = logging.getLogger(__name__)

class NotOnTranscriptException(Exception):
    ''' For when a transcription position is not on a transcript '''
    pass


class Transcript(HTSeq.GenomicFeature):
    FEATURE_TYPE = "transcript"
    BIOTYPE = "gene_biotype"

    def __init__(self, transcript_id, reference):
        super(Transcript, self).__init__(transcript_id, Transcript.FEATURE_TYPE, None)
        self.reference = reference
        self.attr = { }
        self.features_by_type = defaultdict(set)

        # By default end of gene (ie empty)
        self.is_coding   = False
        self.cds_start   = None
        self.cds_end     = None


    def __iadd__(self, feature):
        self.features_by_type[feature.type].add(feature)
        if self.iv is None:
            self.iv = feature.iv.copy()
        else:
            self.iv.extend_to_include(feature.iv)
        if getattr(feature, "attr", None):
            self.attr.update(feature.attr)

        if feature.type in ["CDS", "start_codon", "stop_codon"]:
            self.is_coding = True
            # Continue down here, setting csd_start etc

            if self.cds_start is None:
                self.cds_start = feature.iv.start
            else:
                self.cds_start = min(self.cds_start, feature.iv.start)

            if self.cds_end is None:
                self.cds_end = feature.iv.end
            else:
                self.cds_end = max(self.cds_end, feature.iv.end)

        return self

    def get_id(self):
        return self.name
    
    def get_gene_id(self):
        return self.attr["gene_id"]

    def get_gene_name(self):
        return self.attr["gene_name"]

    def get_biotype(self):
        return self.attr[Transcript.BIOTYPE]

    def get_features(self, feature_type):
        '''features returned in sorted order, 5' -> 3'?'''
        is_reversed = self.iv.strand == '-'
        return sorted(self.features_by_type[feature_type], key=lambda x : int(x.iv.start), reverse=is_reversed)

    def get_transcript_position(self, pos):
        previous_exon_lengths = 0
        for exon in self.get_features("exon"):
            if exon.iv.contains(pos):
                exon_pos = abs(pos.pos - exon.iv.start_d)
                return previous_exon_lengths + exon_pos
            else:
                previous_exon_lengths += exon.iv.length

        raise NotOnTranscriptException("Couldn't find %s in transcript %s exons" % (pos, self.get_id()))

    def get_codon_position(self, feature_type):
        # There could be 2 split across diff exons (aarrgh!) this is rare so do it twice and get the 1st one
        codon_positions = [f.iv.start_d_as_pos for f in self.get_features(feature_type)]
        return min([self.get_transcript_position(p) for p in codon_positions])

    def get_start_codon_position(self):
        return self.get_codon_position("start_codon")

    def get_stop_codon_position(self):
        return self.get_codon_position("stop_codon")

    def get_transcript_length(self):
        return self.get_features_length("exon")

    def get_5putr_length(self):
        return self.get_features_length("5PUTR")

    def get_3putr_length(self):
        return self.get_features_length("3PUTR")

    def get_features_length(self, feature_type):
        return sum([f.iv.length for f in self.get_features(feature_type)])
    
    @Lazy
    def exons(self):
        '''Returns list of exon features'''
        return self.get_features("exon")
    
    @Lazy
    def threeputr(self):
        '''Returns the exon regions which contain 3'UTR as list of features'''
        return self.get_features("3PUTR") 
    
    @Lazy
    def fiveputr(self):
        '''Returns the exon regions which contain 5'UTR as list of features'''
        return self.get_features("5PUTR")
    
    def get_coding_sequence(self):
        ''' Warning: There are frame shift issues not handled here.
            Do not naively turn this into a protein - better to use existing databases '''
        return self.get_sequence_from_features("CDS")

    def get_5putr_sequence(self):
        return self.get_sequence_from_features("5PUTR")

    def get_3putr_sequence(self):
        return self.get_sequence_from_features("3PUTR")

    def get_promoter_sequence(self, promoter_range=None):
        '''range(int) = distance (bp) upstream and downstream from TSS'''
        if promoter_range is None:
            promoter_range = 1000

        iv = self.get_promoter_iv(promoter_range)
        return self.reference.get_sequence_from_iv(iv)
    
    def get_transcript_sequence(self):
        return self.get_sequence_from_features("exon")

    def get_sequence_from_features(self, region):
        return self.reference.get_sequence_from_features(self.get_features(region))
    
    # TODO: Should I move the get_region_extents into get_transcript Position?
    # advantage of get_extends is you pass in an interval so can use both sides
    # (to know at least one side is touching exon)
    def get_transcript_positions(self, iv):
        if not self.is_coding:
            raise NotOnTranscriptException(self.get_id() + " is non-coding")

        (start, end) = self.get_first_and_last_genomic_position_on_transcript(iv)
        if start is None or end is None:
            raise NotOnTranscriptException("Could not determine exon region extents for", iv)

        start_mpos   = self.get_transcript_position(start)
        end_mpos     = self.get_transcript_position(end)
        if self.iv.strand == '-':
            (start_mpos, end_mpos)  = (end_mpos, start_mpos)
        return (start_mpos, end_mpos)

    def get_first_and_last_genomic_position_on_transcript(self, iv):
        ''' returns lowest/greatest point on transcript that intersects with IV '''
        region_intervals = []
        for feature in self.features_by_type["exon"]:
            if iv.overlaps(feature.iv):
                overlap_start = max(iv.start, feature.iv.start)
                overlap_end = min(iv.end, feature.iv.end)

                overlap_iv = HTSeq.GenomicInterval(iv.chrom, overlap_start, overlap_end, iv.strand)
                region_intervals.append(overlap_iv)
                
        start = None
        end = None
        if len(region_intervals) > 0:
            start = region_intervals[0].start_as_pos
            end = genomics.last_base(region_intervals[-1])

        return (start, end)

    def get_promoter_iv(self, promoter_range=None):
        '''default range = +/- 1000 bp of TSS'''
        if promoter_range is None:
            promoter_range = 1000
        return genomics.iv_from_pos_range(self.tss, promoter_range)
    
    @Lazy
    def tss(self):
        return self.iv.start_d_as_pos
       
    def get_promoter_iv_custom_range(self, upstream_distance, downstream_distance):
        '''Get any interval surrounding TSS
        Note: total length of interval = upstream_distance + downstream_distance (The TSS base is included in downstream_distance)'''
        return genomics.iv_from_pos_directional_before_after(self.tss, upstream_distance, downstream_distance)
    
    def get_promoter_sequence_custom_range(self, upstream_distance, downstream_distance):
        iv = self.get_promoter_iv_custom_range(upstream_distance, downstream_distance)
        return self.reference.get_sequence_from_iv(iv)
    
    def get_gene_body_iv_custom_range(self, upstream_distance, downstream_distance):
        return genomics.iv_directional_extend(self.iv, upstream_distance, downstream_distance)
    
    def get_intron_ivs(self):
        '''
        Purpose: Get list of intron intervals for the the transcript
        Output: A list of HTSeq Genomic Interval instances for all introns in the transcript (ordered according to strand)
        '''
        intron_ivs = []
        previous_exon = None
        for exon in self.get_features("exon"): # This is in stranded order
            if previous_exon:
                # HTSeq ends are 1 past the last base of the sequence.
                # Thus for touching sequences like exons/introns, first_seq.end = second_seq.start
                intron_start = previous_exon.iv.end_d
                intron_end = exon.iv.start_d
                intron_length = abs(intron_end - intron_start)
                intron = genomics.GenomicInterval_from_directional(exon.iv.chrom, intron_start, intron_length, exon.iv.strand)
                intron_ivs.append(intron)
            previous_exon = exon
        return intron_ivs
    
    def get_intron_sequences(self):
        '''
        Get list of intron sequences for transcript
        Output: List of sequences (intron order and sequences are 5' to 3')
        '''
        list_of_intron_intervals = self.get_intron_ivs()
        intron_sequences = list()
        for intron in list_of_intron_intervals:
            intron_sequences.append(self.reference.get_sequence_from_iv(intron))
        return intron_sequences

    def get_genomic_position(self, pos_on_transcript):
        '''
        Converts 0-based position on a transcript into 0-based position on the chromosome
        Arguments -- position relative to 5' end of transcript (int) 
        Returns -- position on chromosome (int)
        '''    
        logger.debug("Searching %s for %d", self.get_id(), pos_on_transcript)
        
        running_exon_length = 0
        previous_running_exon_length = 0
        #go through exons in order, adding to running_exon_length, until you find the one with the miR seed start position in it
        for exon in self.get_features("exon"):
            running_exon_length += exon.iv.length
            if pos_on_transcript < running_exon_length: #match start is in this exon
                logger.debug("found the exon, %r running exon length is: %r", exon, running_exon_length)
                logger.debug("previous_running_exon_length is %r:", previous_running_exon_length)
                logger.debug("exon start_d and end_d are: %r, %r", exon.iv.start_d, exon.iv.end_d)
                genomic_pos_of_exon_start = exon.iv.start_d
                pos_on_this_exon = pos_on_transcript - previous_running_exon_length
                logger.debug("position on this exon is %r", pos_on_this_exon)
                if exon.iv.strand == '+':
                    genomic_position_of_match_start = genomic_pos_of_exon_start + pos_on_this_exon
                elif exon.iv.strand == '-':
                    genomic_position_of_match_start = genomic_pos_of_exon_start - pos_on_this_exon
                else:
                    raise ValueError("strand must be + or -, not: %r" % exon.iv.strand)
                return genomic_position_of_match_start
            previous_running_exon_length += exon.iv.length

        raise NotOnTranscriptException("%s didn't contain %s" % (self.get_id(), pos_on_transcript))

    def extract_region_sequence(self, region):
        ''' Convenience method to extract sequence of regions by string '''
        extraction_methods = {"5putr" :         lambda t : t.get_5putr_sequence(),
                              "3putr" :         lambda t : t.get_3putr_sequence(),
                              "transcript" :    lambda t : t.get_transcript_sequence(),
                              "coding" :        lambda t : t.get_coding_sequence(),
                              "cds" :           lambda t : t.get_coding_sequence(),
                              "exons" :         lambda t : t.get_transcript_sequence(),
                              "introns" :       lambda t : t.get_intron_sequences(),
        }
        extract_region = extraction_methods.get(region)
        if extract_region is None:
            raise ValueError("Unknown region %s (valid=%s)" % (region, extraction_methods.keys()))
        return extract_region(self)

    def iv_from_transcript_positions(self, t_start, t_end):
        g_start = self.get_genomic_position(t_start)
        g_end = self.get_genomic_position(t_end)
        logger.debug("transcript pos: %d,%d => genomic pos: %d, %d", t_start, t_end, g_start, g_end)

        if g_start < g_end:
            start, end = g_start, g_end
        else:
            start, end = g_end, g_start
        return HTSeq.GenomicInterval(self.iv.chrom, start, end+1, self.iv.strand)

    def genomic_feature_from_transcript_positions(self, name, feature_type, t_start, t_end):
        iv = self.iv_from_transcript_positions(t_start, t_end)
        feature = HTSeq.GenomicFeature(name, feature_type, iv)
        
        #Bed file formatting: determine whether seed match is over an intron and set feature attributes accordingly (bed file writer will read these -> output pretty).
        transcript_distance = t_end - t_start + 1 # Extra 1 as it's the coordinate of the end base not 1 past
        if feature.iv.length == transcript_distance:
            block_count = "1"
            block_sizes = transcript_distance
            block_starts = "0"
        else: # Genomic distance != transcript_distance (crosses introns)
            logger.debug("Found one! Match is in 2 exons")
            block_count = "2"
            for exon in self.get_features("exon"):
                if feature.iv.start_as_pos.overlaps(exon.iv):
                    block_1_size = exon.iv.end - feature.iv.start
                if feature.iv.end_as_pos.overlaps(exon.iv):
                    block_2_size = feature.iv.end - exon.iv.start
                    block_2_start = exon.iv.start - feature.iv.start #start position relative to seedmatch inverval start
            block_sizes = str(block_1_size) + "," + str(block_2_size)
            block_starts = "0," + str(block_2_start)
        logger.debug("block sizes %r, block starts %r", block_sizes, block_starts)
        feature.attr = {'thick_start': feature.iv.start, 'thick_end':feature.iv.end, 'block_count':block_count, 'block_sizes':block_sizes, 'block_starts':block_starts}
        return feature
