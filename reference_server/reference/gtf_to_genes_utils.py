'''

See: http://gtf-to-genes.googlecode.com/hg/doc/build/html/index.html for gtf_to_genes documentation

gtf_to_genes implementation note:

It would be maybe 5 seconds quicker to load genes in gtf_to_genes, and then construct our Genes/Transcript objects from that.

However, the Transcript object and calling code (eg users of features_by_type[]) rely on exons etc being
GenomicFeature objects, and it is too much work to refactor this out at the moment.

The current implementation is thus to convert BACK to GenomicFeature objects and do the swap-out for straight GTF
loading in reference.transcript method.

TODO: Abstract away exons etc as something else other than GenomicFeatures from client code, so we can make this change

Created on May 8, 2013

@author: dave
'''

from reference_server.reference.transcript import Transcript
import HTSeq
import logging
import os
import pandas as pd
import time

CONVERTED_TO_GTF_TO_GENES_LABEL = ".gtf_to_genes"
DETERMINE_FROM_TRANSCRIPT_ID = "determine_from_transcript_id"

def ucsc_gtf_to_genes_filename(gtf_filename):
    ''' Name of the new GTF after modification for gtf_to_genes '''
    (root, ext) = os.path.splitext(gtf_filename)
    return root + CONVERTED_TO_GTF_TO_GENES_LABEL + ext

class GTFToGenesLoader(object):
    IDENTIFIER_COLUMN = "id"

    def __init__(self, gtf_to_genes_index):
        columns = [GTFToGenesLoader.IDENTIFIER_COLUMN, "filename", "cache"]
        self.gtf_to_genes_index = gtf_to_genes_index
        self.df = pd.read_table(gtf_to_genes_index, sep='\t', names=columns, index_col=1)
        
    def get_identifier(self, gtf_filename):
        ''' Return gtf_to_genes index identifier for a gtf filename '''
        
        if gtf_filename in self.df.index:
            filename = gtf_filename
        else:
            realpath = os.path.realpath(gtf_filename)
            if realpath.find("UCSC") != -1:
                filename = ucsc_gtf_to_genes_filename(realpath)
            else:
                filename = realpath # Ensembl

        try:
            return self.df.ix[filename][GTFToGenesLoader.IDENTIFIER_COLUMN]
        except KeyError:
            message = "Could not find '%s' (asked for '%s') in gtf_to_genes index '%s'" % (filename, gtf_filename, self.gtf_to_genes_index)
            raise ValueError(message)


def load_gtf_to_genes(gtf_file_name, gtf_to_genes_index):
    logger = logging.getLogger(__file__)

    genes = None
    try:
        import gtf_to_genes

        loader = GTFToGenesLoader(gtf_to_genes_index)
        
        identifier = loader.get_identifier(gtf_file_name)
        start = time.time()

        # TODO: Changing directory seems a bit of a hack but gtf_to_genes writes relative paths - can we fix?
        cwd = os.getcwd()
        gtf_to_genes_dir = os.path.dirname(gtf_to_genes_index)
        os.chdir(gtf_to_genes_dir)
        _, _, genes = gtf_to_genes.get_indexed_genes_for_identifier(gtf_to_genes_index,  logger,  identifier) #@UndefinedVariable
        os.chdir(cwd)
        
        logger.info("Loaded GTF to genes in %d secs", (time.time() - start))
    except ImportError:
        logger.info("Could not import gtf_to_genes - please run 'pip install gtf_to_genes'")
    except Exception as e:
        logger.info("Couldn't use gtf to genes: %s", e)

    return genes


def create_transcript(reference, t_transcript):
    ''' Creates sacgf.genomics.reference.Transcript from gtf_to_genes t_transcript '''
    
    transcript = Transcript(t_transcript.cdna_id, reference)
    gene = t_transcript.gene

    strand = ["-", "+"][gene.strand]

    biotype = gene.gene_type
    if biotype == DETERMINE_FROM_TRANSCRIPT_ID:
        biotyper = TranscriptIdBioTyper()
        biotype = biotyper.get_biotype(t_transcript.cdna_id)
    
    attr = {"gene_id" : gene.gene_id,
            "gene_name" : gene.names[0],
            Transcript.BIOTYPE : biotype}
    
#    if transcript.get_id().startswith("NM_") and biotype != 'protein_coding':
        #TODO: figure out why this is the case 
#        print "%s biotype = %s!!" % (transcript.get_id(), biotype)

    # Create a whole bunch of fake features which represent this transcript
    feature_types = {"exon" : (gene.exons, t_transcript.exon_indices),
                     "CDS" : (gene.coding_exons, t_transcript.coding_exon_indices)}
    
    for (feature_type, (exons, indices)) in feature_types.iteritems():
        for i in indices:
            #print "%s exon = %d" % (feature_type, i)
            exon = exons[i]
            iv = HTSeq.GenomicInterval(gene.contig, exon[0]+1, exon[1], strand) # +1 as GTF to genes start is off by 1
            feature = HTSeq.GenomicFeature("", feature_type, iv)
            feature.attr = attr
            transcript += feature

    codon_types = {"start_codon": t_transcript.start_codons,
                   "stop_codon" : t_transcript.stop_codons}

    for (codon_type, codons) in codon_types.iteritems():
        for codon in codons:
            iv = HTSeq.GenomicInterval(gene.contig, codon[0]+1, codon[1], strand) # +1 as GTF to genes start is off by 1
            feature = HTSeq.GenomicFeature("", codon_type, iv)
            feature.attr = attr
            transcript += feature

    return transcript

class DetermineFromTranscriptIdBioTyper(object):
    def set_biotype(self, feature):
        feature.attr[Transcript.BIOTYPE] = DETERMINE_FROM_TRANSCRIPT_ID

    def biotype_assignment_method(self):
        return "Setting to '%s' so we can assign based on transcript_id at load time" % DETERMINE_FROM_TRANSCRIPT_ID

class TranscriptIdBioTyper(object):
    ''' Sets 'biotype' feature attribute to "protein_coding" or "non_coding"
        based on whether transcript_id starts with "NM_" or "NR_" '''
    
    biotypes = {"NM_" : "protein_coding", "NR_" : "non_coding"}
    def __init__(self, feature_attribute="transcript_id"):
        self.feature_attribute = feature_attribute

    def set_biotype(self, feature):
        transcript_id = feature.attr[self.feature_attribute]
        biotype = self.get_biotype(transcript_id)
        feature.attr[Transcript.BIOTYPE] = biotype

    def get_biotype(self, transcript_id):
        for (start, biotype) in TranscriptIdBioTyper.biotypes.iteritems():
            if transcript_id.startswith(start):
                return biotype
            
        if "tRNA" in transcript_id:
            return "tRNA"
        
        return 'Uknown'

    def biotype_assignment_method(self):
        return "Using transcript_id to assign protein_coding/non_coding"
    

class HardcodedBioTyper(object):
    def __init__(self, biotype):
        self.biotype = biotype

    def set_biotype(self, feature):
        feature.attr[Transcript.BIOTYPE] = self.biotype

    def biotype_assignment_method(self):
        return "Hardcoded to %s" % self.biotype
