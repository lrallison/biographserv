'''
Created on Feb 21, 2013

@author: dave
'''

import HTSeq
import re

from reference_server.genomics import stranded_offset, sorted_genomic_positions


class TranscriptRegionExtractor(object):
    '''Pass it a region (e.g. exon/intron/etc)'''
    def __init__(self, region):
        self.region = region
        
    def get_sequence(self, transcript):
        return transcript.extract_region_sequence(self.region)

    def get_description(self):
        return "%s region" % self.region
    
class TranscriptIntervalExtractor(object):
    position_extractor = {"start"   : lambda t : t.iv.start_d_as_pos,
                          "end"     : lambda t : t.iv.end_d_as_pos
    }

    def __init__(self, start, start_offset, end, end_offset):
        self.get_start = self.position_extractor[start]
        self.start_offset = start_offset
        self.get_end = self.position_extractor[end]
        self.end_offset = end_offset

        start_offset_str = "%d" % start_offset if start_offset else ""
        end_offset_str = "%d" % end_offset if end_offset else ""
        self.description = "%s%s to %s%s" % (start, start_offset_str, end, end_offset_str)

    @classmethod
    def from_string_params(cls, start_str, end_str):
        '''Turns a genomic location string into start and offsets'''
        start, start_offset = cls._get_start_and_offset(start_str)
        end, end_offset = cls._get_start_and_offset(end_str)
        return cls(start, start_offset, end, end_offset)

    @classmethod
    def _get_start_and_offset(cls, param):
        pattern = "(" + "|".join(cls.position_extractor.keys()) + r")([+-]\d+)?"

        if param:
            match = re.match(pattern, param, re.IGNORECASE)
            if match:
                key = match.group(1)
                offset = int(match.group(2) or 0)
            else:
                raise ValueError("Error parsing %s" % param)
        else:
            key = "start"
            offset = 0

        return (key, offset)

    def get_sequence(self, transcript):
        start = stranded_offset(self.get_start(transcript), self.start_offset)
        end = stranded_offset(self.get_end(transcript), self.end_offset)
        (start, end) = sorted_genomic_positions(start, end)
        iv = HTSeq.GenomicInterval(start.chrom, start.pos, end.pos, start.strand)
        return transcript.reference.get_sequence_from_iv(iv)
    
    def get_description(self):
        return self.description
