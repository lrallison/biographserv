'''
Utilities to handle Genome Annotations and reference genomes

Because of the large size of the files (500mb genes GTF and >3 gig reference genome) it loads
required files on demand at first access.

Created on Nov 29, 2012

@author: dlawrence
'''

import HTSeq
from collections import defaultdict
import operator
import os
from pyfasta import Fasta  # @UnresolvedImport

from bgs.utils.utils import Lazy
from reference_server import genomics
from reference_server.genomics import chrom_data_access, format_chrom, get_unique_features_from_genomic_array_of_sets_iv, iv_opposite_strand
from reference_server.reference import gtf_to_genes_utils
from reference_server.reference.gene import Gene
from reference_server.reference.mirna import MiRNA
from reference_server.reference.transcript import Transcript


# TODO: Make this work across species (remove hardcoded # of somatic chroms) - and create inside Reference object 
# Maybe iterate through chroms in reference then filter out those that don't match simply number or X/Y?
HUMAN_CHROMOSOMES = set([str(i) for i in range(1, 23)] + ["X", "Y"]) # Now use 'X' instead of 'chrX'

class Reference(object):
    TRNA_BIOTYPE = "tRNA"
    DEFAULT_GENE_IDENTIFIER = "gene_name"
    DEFAULT_TRANSCRIPT_IDENTIFIER = "transcript_id"
    REGION_TYPES = ["coding", "5PUTR", "3PUTR", "non coding", "intron"]
    
    def __init__(self, igenomes_reference, **kwargs):
        self.transcript_id = Reference.DEFAULT_TRANSCRIPT_IDENTIFIER
        self.gene_id = Reference.DEFAULT_GENE_IDENTIFIER
        self.stranded = kwargs.get("stranded", True) # For regions
        self.standard_chromosomes_only = kwargs.get("standard_chromosomes_only", True)

        self.reference_genes = igenomes_reference.genes_gtf
        self.reference_genome = igenomes_reference.genome_fasta
        self.reference_mature_mirna_fasta = igenomes_reference.mature_mirna_fasta 
        self.gtf_to_genes_index = os.path.join(igenomes_reference.igenomes_base_dir, 'gtf.index')
        
    def _load_transcripts_gtf_to_genes(self, genes_by_biotype):
        print "loading via GTF to genes"
        
        transcripts = {}
        for genes in genes_by_biotype.itervalues():
            for g in genes:
                for t in g.transcripts:
                    transcripts[t.cdna_id] = gtf_to_genes_utils.create_transcript(self, t)
        return transcripts      

    def _load_transcripts_from_gtf(self, reference_genes_gtf):
        ''' Loads from gtf and sets biotype (from transcript_id) '''
        reader = HTSeq.GFF_Reader(open(reference_genes_gtf)) # open to handle unicode...
        
        print "GTF: '%s'" % reference_genes_gtf
        genomic_array_of_features = genomics.features_to_genomic_array_of_sets(reader)
        biotyper = gtf_to_genes_utils.TranscriptIdBioTyper(self.transcript_id)

        transcripts = {}
        for (_, features) in genomic_array_of_features.steps():
            for feature in features:
                if not feature.attr.get(Transcript.BIOTYPE):
                    biotyper.set_biotype(feature)
                transcript_id = feature.attr[self.transcript_id]
                transcript = transcripts.get(transcript_id)
                if not transcript:
                    transcript = Transcript(transcript_id, self)
                    transcripts[transcript_id] = transcript
                    
                transcript += feature
        return transcripts

    def _load_transcripts(self, gtf_filename):
        ''' Load transcripts from "gtf_filename" using gtf_to_genes if possible '''
        
        genes_by_biotype = gtf_to_genes_utils.load_gtf_to_genes(gtf_filename, self.gtf_to_genes_index)
        if genes_by_biotype: # gtf_to_genes worked
            transcripts = self._load_transcripts_gtf_to_genes(genes_by_biotype)
        else: # fall back on GTF
            transcripts = self._load_transcripts_from_gtf(gtf_filename)
        return transcripts


    @Lazy
    def transcripts(self):
        ''' dict of {"transcript_id" : Transcript} '''

        transcripts = self._load_transcripts(self.reference_genes)
        if self.standard_chromosomes_only:
            transcripts = {t_name:t for (t_name,t) in transcripts.iteritems() if format_chrom(t.iv.chrom, False) in HUMAN_CHROMOSOMES}
        self.stranded_regions = self.load_regions(transcripts, True)
        self.unstranded_regions = self.load_regions(transcripts, False)
        
        return transcripts

    @Lazy
    def regions(self):
        return self.get_regions()

    def get_regions(self, stranded=None):
        ''' GenomicArray of strings representing regions (Stranded=Pass in, or use reference setting) '''
        
        if stranded is None:
            stranded = self.stranded 
        
        _ = self.transcripts # This will load regions
        if stranded:
            regions = self.stranded_regions
        else:
            regions = self.unstranded_regions
        return regions

    @Lazy
    def genomic_transcripts(self):
        ''' GenomicArrayOfSets containing transcripts - used for lookups '''
        transcripts_ga = HTSeq.GenomicArrayOfSets("auto", self.stranded)
        for t in self.transcripts.values():
            transcripts_ga[t.iv] += t

        return transcripts_ga

    @Lazy
    def _transcripts_for_gene(self):
        ''' dict of {"gene_name" : dict{"biotype" : transcript[]} } 
            This allows us to group transcripts easily, for genes and genes_by_biotype
        '''
        transcripts_for_gene_dict = defaultdict(lambda : defaultdict(list))
        for t in self.transcripts.values():
            gene_name = t.attr[self.gene_id]
            transcripts_for_gene_dict[gene_name][t.get_biotype()].append(t)
        return transcripts_for_gene_dict

    @Lazy
    def genes(self):
        ''' dict of {"gene_name" : Gene} 
            This contains *all* genes '''
        genes_dict = {}

        # Create a gene that represents all transcripts
        for (name, transcripts_per_biotype) in self._transcripts_for_gene.iteritems():
            all_transcripts_for_gene = []
            for transcripts in transcripts_per_biotype.itervalues():
                all_transcripts_for_gene.extend(transcripts)
            genes_dict[name] = Gene(name, all_transcripts_for_gene)

        return genes_dict
    
    @Lazy
    def protein_coding_genes(self):
        '''Return dict of {gene_name: Gene} for protein coding genes'''
        genes_dict = {}
        for gene in self.genes_by_biotype["protein_coding"]:
            genes_dict[gene.name] = gene
        return genes_dict
    
    @Lazy
    def protein_coding_transcripts(self):
        '''Returns dict of {transcript_id: Transcript} for all protein coding transcripts'''
        protein_coding_transcripts = {}
        for transcript_name, transcript in self.transcripts.iteritems():
            if transcript.is_coding:
                protein_coding_transcripts[transcript_name] = transcript
            
        return protein_coding_transcripts
    
    @Lazy
    def genes_by_biotype(self):
        ''' dict of {"biotype" : array_of_genes_biotype }
            This also includes 'tRNA' (from non-standard UCSC GTF) '''

        genes_by_biotype_dict = defaultdict(list)

        # Create a gene that represents all transcripts
        for (name, transcripts_per_biotype) in self._transcripts_for_gene.iteritems():
            for (biotype, transcripts) in transcripts_per_biotype.iteritems():
                gene = Gene(name, transcripts, biotype=biotype)
                genes_by_biotype_dict[biotype].append(gene)

        genes_by_biotype_dict[Reference.TRNA_BIOTYPE].extend(self.trna_genes.itervalues())

        return genes_by_biotype_dict

    @Lazy
    def trna_transcripts(self):
        ''' dict of {"tRNA_name" : Transcript}  '''
        return self._load_transcripts(self.reference_trna)

    @Lazy
    def trna_genes(self):
        ''' dict of {"tRNA_name" : Gene}  '''
        trna_genes = {}
        for transcript in self.trna_transcripts.itervalues():
            gene_name = transcript.get_gene_id()
            gene = Gene(gene_name, [transcript], biotype=Reference.TRNA_BIOTYPE)
            trna_genes[gene_name] = gene
        return trna_genes
    
    def load_regions(self, transcripts, stranded):
        regions = HTSeq.GenomicArray( "auto", stranded=stranded, typecode='O' )

        # Make everything that has a gene in it be "intron"
        for t in transcripts.values():
            regions[t.iv] = "intron"

        # Non-coding
        for t in transcripts.values():
            if not t.is_coding:
                for exon in t.features_by_type["exon"]:
                    regions[exon.iv] = "non coding"

        # Go through and add in gene specific details (overriding introns)
        for t in transcripts.values():
            if t.is_coding:
                # Add untranslated regions (using exons compared to cds_start, cds_end)
                (left, right) = ("5PUTR", "3PUTR")
                if t.iv.strand == '-': # Switch
                    (left, right) = (right, left)
                
                for exon in t.features_by_type["exon"]:
                    if exon.iv.start < t.cds_start:
                        end_non_coding = min(t.cds_start, exon.iv.end)
                        non_coding_interval = HTSeq.GenomicInterval(exon.iv.chrom, exon.iv.start, end_non_coding, exon.iv.strand)
                        regions[non_coding_interval] = left
                        t += HTSeq.GenomicFeature(left, left, non_coding_interval.copy())

                    if exon.iv.end > t.cds_end:
                        start_non_coding = max(t.cds_end, exon.iv.start)
                        non_coding_interval = HTSeq.GenomicInterval(exon.iv.chrom, start_non_coding, exon.iv.end, exon.iv.strand)
                        regions[non_coding_interval] = right
                        t += HTSeq.GenomicFeature(right, right, non_coding_interval.copy())
    
                for coding in t.features_by_type["CDS"]:
                    regions[coding.iv] = "coding"
                
        return regions

    @Lazy
    def genome(self):
        if not self.reference_genome:
            raise Exception("asked for genome sequence file but Reference was not given a .fasta file")

        # @see: https://pypi.python.org/pypi/pyfasta
        key_fn = lambda key : key.split()[0] # Use first value before whitespace as keys
        return Fasta(self.reference_genome, key_fn=key_fn)
    
    @Lazy
    def mirna_mature(self):
        '''Returns dict of { miRNA name: mature miR RNA sequence }'''
        if not self.reference_mature_mirna_fasta:
            raise RuntimeError("asked for mirna mature sequence file but Reference was not given a .fasta file")

        return genomics.fasta_to_hash(self.reference_mature_mirna_fasta)

    def get_mirna(self, mirna_name):
        '''Returns an MiRNA class instance for the mirna_name'''
        mirna = MiRNA(mirna_name, self.mirna_mature)
        return mirna

    def get_transcripts_in_iv(self, interval):
        '''Returns: list of transcripts in genomic interval'''
        transcripts = get_unique_features_from_genomic_array_of_sets_iv(self.genomic_transcripts, interval)
        return list(transcripts)

    def get_transcripts_in_unstranded_iv(self, interval):
        transcripts = get_unique_features_from_genomic_array_of_sets_iv(self.genomic_transcripts, interval)
        antisense_iv = iv_opposite_strand(interval)
        transcripts.update(get_unique_features_from_genomic_array_of_sets_iv(self.genomic_transcripts, antisense_iv))
        return list(transcripts)

    def get_regions_array(self, interval):
        '''Returns: list of region types (str) in the interval'''
        _ = self.transcripts
        regions = []
        for (_, region_name) in self.regions[interval].steps():
            if region_name:
                regions.append(region_name)
        return regions

    def get_transcript_ids(self, interval):
        return [feature.name for feature in self.get_transcripts_in_iv(interval)]

    def get_gene_names_array(self, interval):
        return list(set([feature.attr["gene_id"] for feature in self.get_transcripts_in_iv(interval)]))

    def get_gene(self, gene_id):
        return self.genes[gene_id]

    def get_transcript(self, transcript_id):
        return self.transcripts[transcript_id]

    def get_gene_names(self, interval):
        '''Returns a string of gene names'''
        gene_names = self.get_gene_names_array(interval)
        return " ".join(gene_names)

    def get_region_names(self, interval):
        region_names = set(self.get_regions_array(interval))
        return " ".join(region_names)

    def get_region(self, interval):
        ''' Returns "best" region - if multiple pick according to order of REGION_TYPES '''
        region_names = set(self.get_regions_array(interval))
        region = None
        for r in self.REGION_TYPES:
            if r in region_names:
                region = r
                break
        return region


    def get_sequence_from_iv(self, iv):
        feature = HTSeq.GenomicFeature("foo", "bar", iv)
        return self._get_sequences_from_feature(feature)

    def get_sequence_from_features(self, features):
        sequences = self._get_sequences_from_features(features)
        sequence = ""
        if len(sequences) > 0:
            sequence = reduce(operator.add, sequences)
        return sequence

    def _get_sequences_from_feature(self, feature):
#        seq_str = str(seq_feature.extract(self.genome[chrom].seq)) # Use strings not BioPython Sequences
        return self.genome.sequence(genomics.HTSeqInterval_to_hash(feature.iv), one_based=False)

    def _get_sequences_from_features(self, features):
        sequences = []
        for feature in features:
            sequences.append(self._get_sequences_from_feature(feature))
        return sequences
    
    def get_longest_coding_transcript(self, g_pos):
        '''returns longest coding transcript overlapping a genomic position'''
        longest_coding_transcript = None
        transcript_is_better = lambda t : t.iv.length > longest_coding_transcript.iv.length

        for transcript in self.genomic_transcripts[g_pos]:
            if transcript.is_coding:
                for feature in transcript.features_by_type["exon"]:
                    if feature.iv.contains(g_pos):
                        if longest_coding_transcript is None or transcript_is_better(transcript):
                            longest_coding_transcript = transcript
        return longest_coding_transcript

   
    @Lazy
    def has_chr(self):
        ''' returns True if chromosomes start with chr? '''
        
        some_transcript = self.transcripts.itervalues().next()
        chrom = some_transcript.iv.chrom
        return chrom.startswith("chr")
    
    def get_chrom_dao(self):
        return chrom_data_access.ChromDAO(self.has_chr)
