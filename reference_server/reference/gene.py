'''
Represents a Gene (ie a human readable name for >=1 (overlapping) transcripts)

Created on Dec 31, 2012

@author: dlawrence
'''

import logging

from bgs.utils.utils import Lazy
from reference_server import genomics


class Gene(object):
    '''
    This represents a gene (ie a collection of transcript instances)
    '''
    ALL_BIOTYPES = "ALL_BIOTYPES"
    def __init__(self, name, transcripts, **kwargs):
        ''' name : Gene Name
            transcripts : list of transcripts
            biotype : (optional) transcripts/gene is only of this biotype (default = ALL_BIOTYPES)
        '''
        self.name = name
        self.biotype = kwargs.get("biotype", Gene.ALL_BIOTYPES)
        self.transcripts = transcripts
        self.has_coding_transcript = False
        iv = None
        non_overlapping = 0
        for t in transcripts:
            self.has_coding_transcript |= t.is_coding # If any transcript, set to true
            if iv is None:
                iv = t.iv.copy()
            elif iv.overlaps(t.iv):
                iv.extend_to_include(t.iv)
            else:
                non_overlapping += 1
        
        if non_overlapping:
            logger = logging.getLogger(__name__)
            logger.warn("%s has %d non-overlapping transcripts", name, non_overlapping)

        self.iv = iv

    def __str__(self):
        return "%s" % self.name

    def get_representative_transcript_iv_custom_range(self, upstream_distance, downstream_distance):
        return self.representative_transcript.get_gene_body_iv_custom_range(upstream_distance, downstream_distance)

    def get_gene_body_iv_custom_range(self, upstream_distance, downstream_distance):
        ''' This uses gene.iv (ie greatest extent) not representative transcript '''
        return genomics.iv_directional_extend(self.iv, upstream_distance, downstream_distance)
            
    def get_promoter_iv(self, promoter_range):
        '''promoter_range = distance upstream or downstream from tss, ie. 1/2 of total promoter size
        Note: this promoter iv is from the "representative transcript" '''
        return genomics.iv_from_pos_range(self.get_tss_position(), promoter_range)
        
    def get_promoter_iv_custom_range(self, upstream_distance, downstream_distance):
        return self.representative_transcript.get_promoter_iv_custom_range(upstream_distance, downstream_distance)
    
    def get_promoter_seq_custom_range(self, upstream_distance, downstream_distance):
        return self.representative_transcript.get_promoter_sequence_custom_range(upstream_distance, downstream_distance)

    def get_representative_transcript(self):
        '''Representative transcript = longest coding transcript if gene is coding, otherwise longest transcript
        Returns one Transcript instance or None
        '''
        return self.representative_transcript
        
    @Lazy
    def representative_transcript(self):
        '''Representative transcript = longest coding transcript if gene is coding, otherwise longest transcript
        Returns one Transcript instance or None
        '''
        transcript = self.get_longest_coding_transcript()
        if transcript == None:
            transcript = self.get_longest_transcript()
        return transcript

    @property
    def is_coding(self):
        return self.has_coding_transcript
    
    def get_tss_position(self):
        '''Get TSS of representative transcript'''
        return self.representative_transcript.tss
    
    def get_longest_coding_transcript(self):
        """
        Function: Selects longest coding transcript for a gene
        Returns: Outputs transcript object of the longest coding transcript
        """

        longest_transcript = None
        for transcript in self.transcripts:
            if transcript.is_coding:
                if longest_transcript is None or transcript.get_transcript_length() > longest_transcript.get_transcript_length():
                    longest_transcript = transcript
        return longest_transcript
    
    def get_longest_transcript(self):
        """
        Function: Selects longest transcript for a gene
        Returns: Outputs transcript object of the longest transcript
        """
        longest_transcript = None
        for transcript in self.transcripts:
            if longest_transcript is None or transcript.get_transcript_length() > longest_transcript.get_transcript_length():
                longest_transcript = transcript
        return longest_transcript    

    def __repr__(self):
        return "%s (%s) %d transcripts" % (self.name, self.iv, len(self.transcripts))


    