'''
MicroRNA: A small ~19-22nt non-coding RNA that regulates gene expression, primarily by targeting the 3'UTR

Created in Reference from a .GTF

@see sacgf.genomics.reference.Reference
@see http://www.mirbase.org/

Created on Sep 6, 2012

@author: dlawrence
'''

from Bio.Seq import Seq
from Bio.Alphabet import RNAAlphabet

def rna_to_dna(rna_sequence):
    rna = Seq(rna_sequence, RNAAlphabet())
    return rna.back_transcribe()

def rna_to_target(rna_sequence):
    dna = rna_to_dna(rna_sequence)
    return str(dna.reverse_complement())

class MiRNA(object):
    def __init__(self, name, ref_sequences):
        self.name = name
        self.mature = ref_sequences[self.name]

    def get8mer_target(self):
        return rna_to_target(self.mature[:8])

    # Nucleotides 4-15
    def get_central_paired_target(self):
        return rna_to_target(self.mature[3:15])

    def get_mature_dna(self):
        return str(rna_to_dna(self.mature))



