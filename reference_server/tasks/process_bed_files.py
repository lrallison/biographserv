'''
Created on 03/12/2014

@author: dlawrence
'''
from collections import Counter
import os

from bgs.models import GenomicAnchor
from bgs.parsers import bed_file
from bgs.utils.file_utils import name_from_file_name
import pandas as pd
from reference_server import genomics
from reference_server.genomics import features_to_genomic_array_of_sets


def genomic_array_to_gene_series(reference, anchor, genomic_array, upstream, downstream, field='score'):
    genes_dict = {}

    def get_max_value_from_feature_set(feature_set):
        return max([f.attr[field] for f in feature_set])
    
    for gene_name, gene in reference.genes.iteritems():
        if anchor == GenomicAnchor.TSS:
            iv = gene.get_promoter_iv_custom_range(upstream, downstream)
        elif anchor == GenomicAnchor.GENE_START_END:
            iv = gene.get_gene_body_iv_custom_range(upstream, downstream)
        else:
            msg = "Unknown GenomicAnchor: %s" % anchor            
            raise ValueError(msg)

        if not genomic_array.stranded:
            iv.strand = '.'

        genes_dict[gene_name] = genomics.get_max_iv(genomic_array, iv, get_max_value_from_feature_set)

    return pd.Series(genes_dict)


def genomic_array_regions_series(region_array, genomic_array, empty_key=None):
    '''Counts the number of features in each region
    Returns dict: keys=regions, values=counts per region'''
    counter = Counter()

    for (iv, value) in genomic_array.steps():
        region = empty_key
        if value:
            for (_, ref_value) in region_array[iv].steps():
                if ref_value:
                    region = ref_value
                    if region == "exon":
                        break; # otherwise keep searching...
            counter[region] += 1

    return pd.Series(counter)

def process_bed_file(reference, working_dir, file_name, stranded, params):
    if stranded:
        stranded_str = "stranded"
    else:
        stranded_str = "unstranded"

    chrom_dao = reference.get_chrom_dao()
    bed_reader = chrom_dao.BED_Reader(file_name, stranded=stranded, bed_type='from_filename')
    if bed_reader.bed_type in [bed_file.NARROW_PEAK, bed_file.BROAD_PEAK]:
        field = 'signalValue'
    else:
        field = 'score' 
    
    genomic_array = features_to_genomic_array_of_sets(bed_reader, stranded=stranded)
    base_name = name_from_file_name(file_name)

    gene_scores_data = []
    for data in params:
        name = data["name"]
        anchor = data["anchor"]
        upstream = data['upstream']
        downstream = data['downstream']
        
        gene_description = "%s_%s_%d_%d" % (stranded_str, anchor, upstream, downstream)
        gene_scores_csv = os.path.join(working_dir, '%s.genes_%s.csv' % (base_name, gene_description))
        series = genomic_array_to_gene_series(reference, anchor, genomic_array, upstream, downstream, field)
        series.to_csv(gene_scores_csv)
        gene_scores_data.append({"name" : name,
                                 "gene_scores_csv" : gene_scores_csv})

    regions_csv = os.path.join(working_dir, "%s.regions.csv" % base_name)
    regions_array = reference.get_regions(stranded)
    series = genomic_array_regions_series(regions_array, genomic_array, empty_key='Intergenic')
    series.sort(ascending=False)
    series.to_csv(regions_csv)

    data = {'gene_scores' : gene_scores_data,
            'regions' : regions_csv}
    
    return data