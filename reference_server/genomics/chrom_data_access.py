#!/usr/bin/env python
'''
ACRF Cancer Genomics Facility

Created on 24/09/2013

@author: dlawrence
'''
from bgs.parsers.bed_file import BedFileReader
from reference_server.genomics import BAM_Reader_format_chrom, \
    SAM_Reader_format_chrom


class ChromDAO(object):
    def __init__(self, want_chr):
        self.want_chr = want_chr
    
    def BAM_Reader(self, bam_file_name):
        return BAM_Reader_format_chrom(bam_file_name, self.want_chr)

    def SAM_Reader(self, sam_file_name):
        return SAM_Reader_format_chrom(sam_file_name, self.want_chr)

    def BED_Reader(self, bed_file_name, **kwargs):
        kwargs['want_chr'] = self.want_chr
        return BedFileReader(bed_file_name, **kwargs)
        
