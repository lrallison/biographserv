"""Genomic utilities"""
from bgs.parsers.bed_file import RandomAccessBedFileReader
from reference_server.genomics import BAM_Reader_format_chrom, \
    SAM_Reader_format_chrom, get_with_gzip_extension


#IGV chromosome coordinates obtained by visualisation are 1-based
#GFF chr coordinates are 1-based
#Bed chr coordinates are 0-based
#HTSeq chr coordinates are 0-based
def igv_coord_to_htseq_coord(coordinate):
    '''Converts an IGV chromosome coordinate (1-based) into an HTSeq position (=start) coordinate (0-based)'''
    return (coordinate - 1)

def igv_end_coord_to_htseq_end(coordinate):
    '''Converts an IGV chromosome coordinate (1-based) into an HTSeq-interval-ready end coordinate'''
    return coordinate #-1 to turn IGV coordinate into HTSeq, then +1 to turn into an end coordinate = net result: no change

def igv_start_coord_to_htseq_start(coordinate):
    '''Converts an IGV chromosome coordinate (1-based) into an HTSeq-interval-ready start coordinate (0-based)'''
    return (coordinate - 1)
    
    
def load_alignment_reader(file_name, want_chr):
    ''' Returns a reader which you can iterate over based on file extension
        BAM and BED can be randomly accessed
    '''
    readers = {'.bam' : BAM_Reader_format_chrom,
               '.sam' : SAM_Reader_format_chrom,
               '.bed' : RandomAccessBedFileReader}
    extension = get_with_gzip_extension(file_name)
    clazz = readers.get(extension)
    if not clazz:
        raise ValueError("Unknown alignment format %s" % extension)
    return clazz(file_name, want_chr=want_chr)
