CREATE DATABASE biographserv;
CREATE USER biographserv WITH PASSWORD 'biographserv';
GRANT ALL PRIVILEGES ON DATABASE biographserv to biographserv;

-- Permissions for testing
ALTER DEFAULT PRIVILEGES IN SCHEMA postgres
   GRANT SELECT ON TABLES TO biographserv;

ALTER USER biographserv CREATEDB;
